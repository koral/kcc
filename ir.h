#ifndef __IR_H__
#define __IR_H__

#include "dag.h"
#include "mem.h"

typedef enum {
	IR_FRAG_INST,
	IR_FRAG_LABEL,
        IR_FRAG_STR
} ir_frag_t;

/* ir operands kind of types */
typedef enum {
	IR_VOID,
	IR_SYM,
	IR_CONST,
	IR_LABEL
} ir_operand_t;

/* ir operator types */
#define FOREACH_ir_op_t(ir_op_t)			\
	ir_op_t(IR_INIT_OP = 256)			\
	ir_op_t(IR_GOTO_OP)				\
	ir_op_t(IR_MEM_OP)				\
	ir_op_t(IR_LOAD_OP)				\
	ir_op_t(IR_LDST_OP)				\
	ir_op_t(IR_STORE_OP)				\
	ir_op_t(IR_STRST_OP)				\
	ir_op_t(IR_FUNC_OP)				\
	ir_op_t(IR_ENDFUNC_OP)				\
	ir_op_t(IR_CALL_OP)				\
	ir_op_t(IR_PASS_OP)				\
	ir_op_t(IR_PARAM_OP)				\
	ir_op_t(IR_RET_OP)				\
	ir_op_t(IR_IFEQ_OP)				\
	ir_op_t(IR_IFGE_OP)				\
	ir_op_t(IR_IFLE_OP)				\
	ir_op_t(IR_IFNEQ_OP)				\
	ir_op_t(IR_IFGEQ_OP)				\
	ir_op_t(IR_IFLEQ_OP)				\
	ir_op_t(IR_AND_OP)				\
	ir_op_t(IR_OR_OP)				\
	ir_op_t(IR_REF_OP)				\
	ir_op_t(IR_LAST_OP)

typedef enum {
FOREACH_ir_op_t(GENERATE_ENUM)
} ir_op_t;

static const char *ir_op_t_str[] = {
	FOREACH_ir_op_t(GENERATE_STRING)
};

static inline char *sprint_ir_op_t(char *str,  ir_op_t t)
{
	if (t <= 255) {
		*str++ = t;
	}
	else {
		char const *c = ir_op_t_str[t - IR_INIT_OP] + 3;
		while (*c != '_') {
			*str = *c;
			str++;
			c++;
		}
	}
	*str = 0;

	return str;
}

/* one ir operands*/
struct ir_operand_s {
	ir_operand_t type;
	union {
		struct {
			struct sym_s *sym;
			bool death;	/* when a sym is not live */
		};
		struct const_s imm;
		char *label;
	};
};

/* 3 addr statement */
struct ir_statment_s {
	struct ir_statment_s *prev;
	struct ir_statment_s *next;
	ir_op_t op;
	ir_frag_t type;
	unsigned src_line;
	union {
		char *label;
		char *str;
		struct {
			struct ir_operand_s res;
			struct ir_operand_s arg[2];
		};
	};
};

static inline bool ir_is_frag_branch(struct ir_statment_s *frag)
{
	switch (frag->op) {
	case IR_GOTO_OP:
	case IR_IFEQ_OP:
	case IR_IFGE_OP:
	case IR_IFLE_OP:
	case IR_IFNEQ_OP:
	case IR_IFGEQ_OP:
	case IR_IFLEQ_OP:
		return true;
	default:
		return false;
	}
}

struct ir_frag_s {
	struct ir_statment_s *first;
	struct ir_statment_s *last;
};

extern struct ir_frag_s ir_code;

char *gen_label(char *prefix);

ir_op_t ir_invert_cmp_op(ir_op_t op);

ir_op_t ir_reverse_cmp_op(ir_op_t op);

void dag_to_ir(void);

void ir_dump(void);

void ir_free(void);

static inline struct ir_statment_s *frag_alloc(void)
{
	struct ir_statment_s *frag;

	frag = xcalloc(sizeof(*frag));

	return frag;
}

static inline void ir_add_frag_after(struct ir_statment_s *new_frag,
		       struct ir_statment_s *frag)
{
	struct ir_statment_s *next = frag->next;

	frag->next = new_frag;

	if (next)
		next->prev = new_frag;
	new_frag->prev = frag;
	new_frag->next = next;
}

static inline void ir_frag_rm(struct ir_statment_s *frag)
{
	struct ir_statment_s *prev = frag->prev;
	struct ir_statment_s *next = frag->next;

	if (prev)
		prev->next = next;
	if (next)
		next->prev = prev;

	free(frag);
}

#endif
