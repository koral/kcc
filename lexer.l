%{
#include <string.h>
#include "kcc.h"
#include "parser.h"
#include "err.h"
#include "mem.h"

void yyerror(const char *str);

char *str_cp (const char *in)
{
	size_t len = strnlen(in, MAX_LIT_LEN + 1);
	if (len == MAX_LIT_LEN + 1)
		err_fatal(NULL, "litteral exceding max length");
	char* buf = xmalloc(len + 1);
	strncpy(buf, in, len);
	buf[len] = '\0';

	return buf;
}

%}

%e  1019
%p  2807
%n  371
%k  284
%a  1213
%o  1117

%option outfile="lexer.c" header-file="lexer.h"
%option yylineno
%option nounput


O   [0-7]
D   [0-9]
NZ  [1-9]
L   [a-zA-Z_]
A   [a-zA-Z_0-9]
H   [a-fA-F0-9]
HP  (0[xX])
E   ([Ee][+-]?{D}+)
P   ([Pp][+-]?{D}+)
FS  (f|F|l|L)
IS  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP  (u|U|L)
SP  (u8|u|U|L)
ES  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS  [ \t\v\n\f]

%{
#include <stdio.h>
#include "parser.h"

extern int sym_type(const char *);  /* returns type from symbol table */

#define sym_type(identifier) IDENTIFIER /* with no symbol table, fake it */

static void comment(void);
static int check_type(void);
%}

%%
"/*"					{ comment(); }
"//".*					  { /* consume //-comment */ }

"auto"					{ yylval.tk.line = yylineno; return(AUTO); }
"case"					{ yylval.tk.line = yylineno; return(CASE); }
"char"					{ yylval.tk.line = yylineno; return(CHAR); }
"const"					{ yylval.tk.line = yylineno; return(CONST); }
"continue"				{ yylval.tk.line = yylineno; return(CONTINUE); }
"default"				{ yylval.tk.line = yylineno; return(DEFAULT); }
"do"					{ yylval.tk.line = yylineno; return(DO); }
"double"				{ yylval.tk.line = yylineno; return(DOUBLE); }
"else"					{ yylval.tk.line = yylineno; return(ELSE); }
"enum"					{ yylval.tk.line = yylineno; return(ENUM); }
"extern"				{ yylval.tk.line = yylineno; return(EXTERN); }
"float"					{ yylval.tk.line = yylineno; return(FLOAT); }
"for"					{ yylval.tk.line = yylineno; return(FOR); }
"goto"					{ yylval.tk.line = yylineno; return(GOTO); }
"if"					{ yylval.tk.line = yylineno; return(IF); }
"inline"				{ yylval.tk.line = yylineno; return(INLINE); }
"int"					{ yylval.tk.line = yylineno; return(INT); }
"long"					{ yylval.tk.line = yylineno; return(LONG); }
"register"				{ yylval.tk.line = yylineno; return(REGISTER); }
"restrict"				{ yylval.tk.line = yylineno; return(RESTRICT); }
"return"				{ yylval.tk.line = yylineno; return(RETURN); }
"short"					{ yylval.tk.line = yylineno; return(SHORT); }
"signed"				{ yylval.tk.line = yylineno; return(SIGNED); }
"sizeof"				{ yylval.tk.line = yylineno; return(SIZEOF); }
"static"				{ yylval.tk.line = yylineno; return(STATIC); }
"struct"				{ yylval.tk.line = yylineno; return(STRUCT); }
"switch"				{ yylval.tk.line = yylineno; return(SWITCH); }
"typedef"				{ yylval.tk.line = yylineno; return(TYPEDEF); }
"union"					{ yylval.tk.line = yylineno; return(UNION); }
"unsigned"				{ yylval.tk.line = yylineno; return(UNSIGNED); }
"void"					{ yylval.tk.line = yylineno; return(VOID); }
"volatile"				{ yylval.tk.line = yylineno; return(VOLATILE); }
"while"					{ yylval.tk.line = yylineno; return(WHILE); }
"_Alignas"				{ yylval.tk.line = yylineno; return ALIGNAS; }
"_Alignof"				{ yylval.tk.line = yylineno; return ALIGNOF; }
"_Atomic"				{ yylval.tk.line = yylineno; return ATOMIC; }
"_Bool"					{ yylval.tk.line = yylineno; return BOOL; }
"_Complex"				{ yylval.tk.line = yylineno; return COMPLEX; }
"_Generic"				{ yylval.tk.line = yylineno; return GENERIC; }
"_Imaginary"				{ yylval.tk.line = yylineno; return IMAGINARY; }
"_Noreturn"				{ yylval.tk.line = yylineno; return NORETURN; }
"_Static_assert"			{ yylval.tk.line = yylineno; return STATIC_ASSERT; }
"_Thread_local"				{ yylval.tk.line = yylineno; return THREAD_LOCAL; }
"__func__"				{ yylval.tk.line = yylineno; return FUNC_NAME; }

{L}{A}*					{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno;
						return check_type();
					}

{HP}{H}+{IS}?				{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno;
						return I_CONSTANT;
					}
{NZ}{D}*{IS}?				{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno;
						return I_CONSTANT; }
"0"{O}*{IS}?				{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno;
						return I_CONSTANT;
					}
{CP}?"'"([^'\\\n]|{ES})+"'"		{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno;
						return I_CONSTANT;
					}

{D}+{E}{FS}?				{ yylval.tk.line = yylineno; return F_CONSTANT; }
{D}*"."{D}+{E}?{FS}?			{ yylval.tk.line = yylineno; return F_CONSTANT; }
{D}+"."{E}?{FS}?			{ yylval.tk.line = yylineno; return F_CONSTANT; }
{HP}{H}+{P}{FS}?			{ yylval.tk.line = yylineno; return F_CONSTANT; }
{HP}{H}*"."{H}+{P}{FS}?			{ yylval.tk.line = yylineno; return F_CONSTANT; }
{HP}{H}+"."{P}{FS}?			{ yylval.tk.line = yylineno; return F_CONSTANT; }

({SP}?\"([^"\\\n]|{ES})*\"{WS}*)+	{
						yylval.s.str = str_cp(yytext);
						yylval.s.line = yylineno; return STRING_LITERAL;
					}

"..."					{ yylval.tk.line = yylineno; return ELLIPSIS; }
">>="					{ yylval.tk.line = yylineno; return RIGHT_ASSIGN; }
"<<="					{ yylval.tk.line = yylineno; return LEFT_ASSIGN; }
"+="					{ yylval.tk.line = yylineno; return ADD_ASSIGN; }
"-="					{ yylval.tk.line = yylineno; return SUB_ASSIGN; }
"*="					{ yylval.tk.line = yylineno; return MUL_ASSIGN; }
"/="					{ yylval.tk.line = yylineno; return DIV_ASSIGN; }
"%="					{ yylval.tk.line = yylineno; return MOD_ASSIGN; }
"&="					{ yylval.tk.line = yylineno; return AND_ASSIGN; }
"^="					{ yylval.tk.line = yylineno; return XOR_ASSIGN; }
"|="					{ yylval.tk.line = yylineno; return OR_ASSIGN; }
">>"					{ yylval.tk.line = yylineno; return RIGHT_OP; }
"<<"					{ yylval.tk.line = yylineno; return LEFT_OP; }
"++"					{ yylval.tk.line = yylineno; return INC_OP; }
"--"					{ yylval.tk.line = yylineno; return DEC_OP; }
"->"					{ yylval.tk.line = yylineno; return PTR_OP; }
"&&"					{ yylval.tk.line = yylineno; return AND_OP; }
"||"					{ yylval.tk.line = yylineno; return OR_OP; }
"<="					{ yylval.tk.line = yylineno; return LE_OP; }
">="					{ yylval.tk.line = yylineno; return GE_OP; }
"=="					{ yylval.tk.line = yylineno; return EQ_OP; }
"!="					{ yylval.tk.line = yylineno; return NE_OP; }
";"					{ yylval.tk.line = yylineno; return ';'; }
("{"|"<%")				{ yylval.tk.line = yylineno; return '{'; }
("}"|"%>")				{ yylval.tk.line = yylineno; return '}'; }
","					{ yylval.tk.line = yylineno; return ','; }
":"					{ yylval.tk.line = yylineno; return ':'; }
"="					{ yylval.tk.line = yylineno; return '='; }
"("					{ yylval.tk.line = yylineno; return '('; }
")"					{ yylval.tk.line = yylineno; return ')'; }
("["|"<:")				{ yylval.tk.line = yylineno; return '['; }
("]"|":>")				{ yylval.tk.line = yylineno; return ']'; }
"."					{ yylval.tk.line = yylineno; return '.'; }
"&"					{ yylval.tk.line = yylineno; return '&'; }
"!"					{ yylval.tk.line = yylineno; return '!'; }
"~"					{ yylval.tk.line = yylineno; return '~'; }
"-"					{ yylval.tk.line = yylineno; return '-'; }
"+"					{ yylval.tk.line = yylineno; return '+'; }
"*"					{ yylval.tk.line = yylineno; return '*'; }
"/"					{ yylval.tk.line = yylineno; return '/'; }
"%"					{ yylval.tk.line = yylineno; return '%'; }
"<"					{ yylval.tk.line = yylineno; return '<'; }
">"					{ yylval.tk.line = yylineno; return '>'; }
"^"					{ yylval.tk.line = yylineno; return '^'; }
"|"					{ yylval.tk.line = yylineno; return '|'; }
"?"					{ yylval.tk.line = yylineno; return '?'; }

{WS}+					{ /* whitespace separates tokens */ }
.					{ /* discard bad characters */ }

%%

int yywrap(void)	/* called at end of input */
{
    return 1;		/* terminate now */
}

static void comment(void)
{
    int c;

    while ((c = input()) != 0)
	if (c == '*')
	{
	    while ((c = input()) == '*')
		;

	    if (c == '/')
		return;

	    if (c == 0)
		break;
	}
    yyerror("unterminated comment");
}

static int check_type(void)
{
    switch (sym_type(yytext))
    {
    case TYPEDEF_NAME:		      /* previously defined */
	return TYPEDEF_NAME;
    case ENUMERATION_CONSTANT:	      /* previously defined */
	return ENUMERATION_CONSTANT;
    default:			      /* includes undefined */
	return IDENTIFIER;
    }
}

void yyerror(const char *str)
{
	curr_loc.l = yylineno;
	err_fatal(NULL, str);
}
