#include <inttypes.h>
#include "ir.h"

struct ir_frag_s ir_code;

bool emitting_code;

static void ir_emit_pre(struct node_s *node);

static void ir_emit_post(struct node_s *node);

static void ir_emit_bool_cmp(struct node_s *cond, struct node_s *body_true,
			     struct node_s *body_false);

static void ir_emit_bool_and(struct node_s *node, struct node_s *body_true,
			     struct node_s *body_false);

static void ir_emit_bool_or(struct node_s *node, struct node_s *body_true,
			    struct node_s *body_false);

static void ir_emit_if(struct node_s *node);

static void ir_emit_while_do(struct node_s *node);

static void ir_emit_do_while(struct node_s *node);

static unsigned __syms_acc;

void ir_sym_acc_callback(void *p0, void *p1)
{
	__syms_acc++;
}

static unsigned ir_count_sym_curr_func(void)
{
	__syms_acc = 0;

	sym_walk_curr_scope(ir_sym_acc_callback);

	return __syms_acc;
}

static ir_op_t dag_to_ir_op(op_t op)
{
	if (op <= 256 && (op != '>' && op != '<'))
		return op;
	switch ((int)op) {
	case '>':
		return IR_IFGE_OP;
	case '<':
		return IR_IFLE_OP;
	case K_LE_OP:
		return IR_IFLEQ_OP;
	case K_GE_OP:
		return IR_IFGEQ_OP;
	case K_EQ_OP:
		return IR_IFEQ_OP;
	case K_NE_OP:
		return IR_IFNEQ_OP;
	default:
		ASSERT(0);
		break;
	}
}

static void ir_emit_frag(struct ir_statment_s *frag)
{
	frag->src_line = curr_loc.l;

	if (!ir_code.first)
		ir_code.first = frag;
	else
		ir_code.last->next = frag;

	frag->prev = ir_code.last;
	ir_code.last = frag;
}

static void ir_emit_frag_label(char *l)
{
	struct ir_statment_s *frag;

	if (!l)
		return;

	frag = frag_alloc();
	frag->type = IR_FRAG_LABEL;
	frag->label = l;
	ir_emit_frag(frag);
}

static void ir_emit_frag_goto(char *l)
{
	struct ir_statment_s *inst;

	inst = frag_alloc();
	inst->op = IR_GOTO_OP;
	inst->res.type = IR_LABEL;
	inst->res.label = l;
	ir_emit_frag(inst);
}

static void ir_emit_frag_str(char *c)
{
	char *p;
	struct ir_statment_s *inst;

	p = xmalloc(MAX_TMP_STR);
	strcpy(p, c);

	inst = frag_alloc();
	inst->type = IR_FRAG_STR;
	inst->str = p;
	ir_emit_frag(inst);
}

static char *gen_tmp_name(void)
{
	static unsigned i;
	char *s = xmalloc(MAX_TMP_STR);

	snprintf(s, MAX_TMP_STR, "$%d", i++);

	return s;
}

static void ir_inst_set_oper(struct node_s *node, struct ir_operand_s *oper)
{
	switch (TYPE(node)) {
		case CONST_T:
			oper->type = IR_CONST;
			oper->imm = node->constant;
			break;
		case ID_T:
		case OPR_T:
			oper->type = IR_SYM;
			oper->sym = node->sym;
			break;
		default:
			break;
		}
}

static void ir_inst_set_2args(struct node_s *node, struct ir_statment_s *inst)
{
	unsigned i;

	for (i = 0; i < 2; i++)
		ir_inst_set_oper(CHILD(node, i), &inst->arg[i]);
}

static void ir_emit_static(void *s, void *p)
{
	struct sym_s *sym;
	unsigned n;

	sym = s;
	n = sym->size;

	if (sym->c_type.orig == VAR_O ||
	    sym->c_type.orig == ARR_O) {
		ir_emit_frag_label(sym->name);
		if (!sym->val) {
			if (sym->c_type.ref_lev) {
				/* that's a pointer */
				ir_emit_frag_str("\t.word 0");
				return;
			}
			switch (type_size(&sym->c_type)) {
			case 1:
				while(n--)
					ir_emit_frag_str("\t.byte 0");
				break;
			case 2:
				while(n--)
					ir_emit_frag_str("\t.half 0");
				break;
			case 4:
				while(n--)
					ir_emit_frag_str("\t.word 0");
				break;
			case 8:
				while(n--)
					ir_emit_frag_str("\t.word 0\n\t.word 0");
				break;
			default:
				ASSERT(false);
				break;
			}
		}
		else {
			struct const_s *val;
			char *c = alloca(MAX_TMP_STR);

			val = sym->val;
			if (sym->c_type.ref_lev) {
				/* initialized pointer*/
				if (sym->val->str) {
					char *l = gen_label("l");

					sprintf(c, "\t.word %s",  l);
					ir_emit_frag_str(c);

					ir_emit_frag_label(l);
					snprintf(c, MAX_TMP_STR, "\t.ascii %s",
						 sym->val->str);
					ir_emit_frag_str(c);
				} else {
					sprintf(c, "\t.word 0x%x", val->int32);
					ir_emit_frag_str(c);
				}
				return;
			}
			switch (type_size(&sym->c_type)) {
			case 1:
				while(n--) {
					sprintf(c, "\t.byte 0x%x", val->int8);
					ir_emit_frag_str(c);
					val++;
				}
				break;
			case 2:
				while(n--) {
					sprintf(c, "\t.half 0x%x", val->int16);
					ir_emit_frag_str(c);
					val++;
				}

				break;
			case 4:
				while(n--) {
					sprintf(c, "\t.word 0x%x", val->int32);
					ir_emit_frag_str(c);
					val++;
				}
				break;
			case 8:
				while(n--) {
					sprintf(c, "\t.word 0x%x", (uint32_t)((val->int64 &
								     (0xffffffffLLU << 32)) >> 32));
					ir_emit_frag_str(c);
					sprintf(c, "\t.word 0x%x", (uint32_t)val->int64);
					ir_emit_frag_str(c);
					val++;
				}
				break;
			default:
				ASSERT(false);
				break;
			}
		}
	}
}

static void ir_emit_static_section(void)
{

	sym_walk_curr_scope(ir_emit_static);
}

static void ir_emit_point_ari(struct node_s *node)
{
	struct node_s *base_p;
	struct node_s *off_p;

	if (CHILD(node, 0)->c_type.ref_lev) {
		base_p = CHILD(node, 0);
		off_p = CHILD(node, 1);
	}
	else {
		base_p = CHILD(node, 1);
		off_p = CHILD(node, 0);
	}

	if (TYPE(off_p) == CONST_T) {
		struct ir_statment_s *inst;

		inst = frag_alloc();
		inst->res.type = IR_SYM;
		node->sym = inst->res.sym =
                    sym_add_curr_scope(gen_tmp_name(),
				       REG_T,
				       base_p->c_type.t,
				       0, TMP_O, 1, NULL);

		inst->op = dag_to_ir_op(OP(node));
		ir_inst_set_oper(base_p, &inst->arg[0]);
		inst->arg[1].type = IR_CONST;
		inst->arg[1].imm.u_int64 =
			type_base_size(&base_p->c_type) *
			off_p->constant.u_int64;
		ir_emit_frag(inst);
	}
	else {
		struct ir_statment_s *inst[2];

		inst[0] = frag_alloc();
		inst[0]->res.type = IR_SYM;
		node->sym =
                    inst[0]->res.sym =
			sym_add_curr_scope(gen_tmp_name(), REG_T, base_p->c_type.t,
					   0, TMP_O, 1, NULL);
		inst[0]->op = '*';
		ir_inst_set_oper(off_p, &inst[0]->arg[0]);
		inst[0]->arg[1].type = IR_CONST;
		inst[0]->arg[1].imm.u_int64 = type_base_size(&base_p->c_type);
		ir_emit_frag(inst[0]);
	}
}

static void ir_emit_param_push(struct node_s *node)
{
	int i;
	struct ir_statment_s *inst;

        if (node->sym || TYPE(node) == CONST_T) {
		inst = frag_alloc();
		inst->op = IR_PASS_OP;
		ir_inst_set_oper(node, &inst->res);
		ir_emit_frag(inst);
	} else if (TYPE(node) != OPR_T) {
		return;
	} else {
		for (i = 0; i < node->op.nops; i++)
			ir_emit_param_push(CHILD(node, i));
	}
}

static void ir_emit_assign(struct node_s *node)
{
	struct ir_statment_s *inst;

	if (TYPE(CHILD(node, 0)) == OPR_T) {
		struct sym_s *r_sym;
		ASSERT(OP(CHILD(node, 0)) == K_DE_REF_OP);
		/* array access */
		if ((CHILD(node, 1))->sym) {
			r_sym = (CHILD(node, 1))->sym;
		} else {
			inst = frag_alloc();
			inst->op = '=';
			inst->res.type = IR_SYM;
			inst->res.sym =
				sym_add_curr_scope(gen_tmp_name(),
						   REG_T,
						   CHILD(node, 1)->c_type.t,
						   0, TMP_O, 1, NULL);
			ir_inst_set_oper(CHILD(node, 1), &inst->arg[0]);
			r_sym = inst->res.sym;
			ir_emit_frag(inst);
		}
		dag_walk_resume(CHILD(node, 0), ir_emit_pre,
				ir_emit_post);
		inst = frag_alloc();
		inst->op = IR_STORE_OP;
		ir_inst_set_oper(CHILD(CHILD(node, 0), 0), &inst->arg[0]);
		inst->res.type = IR_SYM;
		node->sym = inst->res.sym = r_sym;
		ir_emit_frag(inst);
	} else {
		inst = frag_alloc();
		inst->op = '=';
		ir_inst_set_oper(CHILD(node, 0), &inst->res);
		ir_inst_set_oper(CHILD(node, 1), &inst->arg[0]);
		node->sym = inst->res.sym;
		ir_emit_frag(inst);
	}

	if (CHILD(node, 0)->sym &&
		(CHILD(node, 0)->sym->st_class == EXT_T ||
		 CHILD(node, 0)->sym->st_class == STATIC_T)) {
		inst = frag_alloc();
		inst->op = IR_STRST_OP;
		inst->arg[0].type = IR_SYM;
		inst->arg[0].sym = CHILD(node, 0)->sym;
		ir_emit_frag(inst);
	}
}

static void ir_fixup_func_inst(struct ir_statment_s *frag)
{
	unsigned loc_syms = ir_count_sym_curr_func();
	unsigned params = 0;

	while (frag && frag->op != IR_FUNC_OP) {
		if (frag->op == IR_PARAM_OP)
			params++;
		frag = frag->prev;
	}

	ASSERT(frag);

	frag->arg[0].type = IR_CONST;
	frag->arg[0].imm.u_int64 = loc_syms - params;
	frag->arg[1].type = IR_CONST;
	frag->arg[1].imm.u_int64 = params;
}

static void ir_emit_post(struct node_s *node)
{
	struct ir_statment_s *inst;

	if (!emitting_code)
		return;

	if (TYPE(node) == ID_T &&
		node->l_val == false &&
		node->sym->c_type.orig == VAR_O &&
		(node->sym->st_class == EXT_T ||
			node->sym->st_class == STATIC_T)) {
		inst = frag_alloc();
		inst->op = IR_LDST_OP;
		inst->arg[0].type = IR_SYM;
		inst->arg[0].sym = node->sym;
		ir_emit_frag(inst);
	}

	if (TYPE(node) != OPR_T)
		return;

	switch ((int)OP(node)) {
	case '+':
	case '-':
		inst = NULL;
		if (CHILD(node, 0)->c_type.ref_lev ||
		    CHILD(node, 1)->c_type.ref_lev) {
			ir_emit_point_ari(node);
			break;
		}
	case '*':
	case '/':
	case '&':
	case '^':
	case '%':
	case '|':
		inst = frag_alloc();
		inst->op = dag_to_ir_op(OP(node));
		node->sym = inst->res.sym =
			sym_add_curr_scope(gen_tmp_name(), REG_T,
					   node->c_type.t, 0, TMP_O, 1, NULL);
		inst->res.type = IR_SYM;
		if (node->op.nops == 2)
			ir_inst_set_2args(node, inst);
		else
			ir_inst_set_oper(CHILD(node, 0), &inst->arg[0]);
		ir_emit_frag(inst);
		break;
	case K_AND_OP:
	case K_OR_OP:
		break;
	case '<':
	case '>':
	case K_LE_OP:
	case K_GE_OP:
	case K_EQ_OP:
	case K_NE_OP: {
		struct node_s *body_true, *body_false;

		node->sym = sym_add_curr_scope(gen_tmp_name(), REG_T,
					       node->c_type.t, 0, TMP_O, 1,
					       NULL);

		body_true = mk_node('=', node->src_line, true, 2, node,
				    mk_leaf_const(1, INT_T, node->src_line));
		body_false = mk_node('=', node->src_line, true, 2, node,
				    mk_leaf_const(0, INT_T, node->src_line));

		ir_emit_bool_cmp(node, body_true, body_false);
		break;
	}
	case '=':
		ir_emit_assign(node);
		break;
        case K_DE_REF_OP:
		if (node->l_val)
			/* we'll emit at assign node level */
			break;
		inst = frag_alloc();
		node->sym = inst->res.sym =
			sym_add_curr_scope(gen_tmp_name(), REG_T,
					   node->c_type.t, 0, TMP_O, 1, NULL);
		inst->op = IR_LOAD_OP;
		ir_inst_set_oper(CHILD(node, 0), &inst->arg[0]);
		ir_inst_set_oper(node, &inst->res);
		ir_emit_frag(inst);
		break;
        case K_REF_OP:
		node->sym = CHILD(node, 0)->sym;
		inst = frag_alloc();
		node->sym = inst->res.sym =
			sym_add_curr_scope(gen_tmp_name(), REG_T,
					   node->c_type.t, 0, TMP_O, 1, NULL);
		inst->op = IR_REF_OP;
		ir_inst_set_oper(CHILD(node, 0), &inst->arg[0]);
		ir_inst_set_oper(node, &inst->res);
		ir_emit_frag(inst);
		break;
	case K_PARAM_DECL_OP:
		inst = frag_alloc();
		inst->op = IR_PARAM_OP;
		inst->res.type = IR_SYM;
		inst->res.sym = CHILD(node, 1)->sym;
		ir_emit_frag(inst);
		break;
	case K_FUNC_CALL_OP:
		if (node->op.nops > 1)
			/* arguments are present */
			ir_emit_param_push(CHILD(node, 1));
		inst = frag_alloc();
		inst->op = IR_CALL_OP;
		node->sym = inst->res.sym =
			sym_add_curr_scope(gen_tmp_name(), REG_T,
					   node->c_type.t, 0, TMP_O, 1, NULL);
		inst->res.type = IR_SYM;
		ir_inst_set_oper(CHILD(node, 0), &inst->arg[0]);
		ir_emit_frag(inst);
		break;
	case K_FUNC_DECL_OP:
		if (ir_code.last->op != IR_RET_OP) {
			inst = frag_alloc();
			inst->op = IR_RET_OP;
			ir_emit_frag(inst);
		}
		inst = frag_alloc();
		inst->op = IR_ENDFUNC_OP;
		ir_emit_frag(inst);
		ir_fixup_func_inst(inst);
		break;
	case K_RETURN_OP:
		inst = frag_alloc();
		inst->op = IR_RET_OP;
		if (node->op.nops)
			ir_inst_set_oper(CHILD(node, 0), &inst->res);
		ir_emit_frag(inst);
		break;
      	default:
		break;
	}
}

static void ir_emit_pre(struct node_s *node)
{
	struct ir_statment_s *inst;

	if (TYPE(node) != OPR_T)
		return;

	if ((int)OP(node) == K_FUNC_DECL_OP)
		emitting_code = true;

	if (!emitting_code)
		return;

	switch ((int)OP(node)) {
	case K_IF_ELSE_OP:
		ir_emit_if(node);
		break;
	case K_OR_OP:
	case K_AND_OP: {
		struct node_s *body_true, *body_false;

		node->sym = sym_add_curr_scope(gen_tmp_name(), REG_T,
					       node->c_type.t, 0, TMP_O, 1,
					       NULL);

		body_true = mk_node('=', node->src_line, true, 2, node,
				    mk_leaf_const(1, INT_T, node->src_line));
		body_false = mk_node('=', node->src_line, true, 2, node,
				    mk_leaf_const(0, INT_T, node->src_line));

		if (OP(node) == K_AND_OP)
			ir_emit_bool_and(node, body_true, body_false);
		else
			ir_emit_bool_or(node, body_true, body_false);
		mark_node_walked(CHILD(node, 0));
		mark_node_walked(CHILD(node, 1));
		break;
	}
	case K_FUNC_DECL_OP:
		inst = frag_alloc();
		inst->op = IR_FUNC_OP;
		inst->res.type = IR_SYM;
		inst->res.sym = CHILD(node, 1)->sym;
		ir_emit_frag(inst);
		break;
	case K_WHILE_OP:
		ir_emit_while_do(node);
		break;
	case K_DO_WHILE_OP:
		ir_emit_do_while(node);
		break;
	case K_POSTFIX_OP:
		node->sym = sym_add_curr_scope(gen_tmp_name(), REG_T,
					CHILD(node, 0)->c_type.t, 0, TMP_O, 1,
					NULL);
		inst = frag_alloc();
		inst->op = '=';
		inst->res.type = IR_SYM;
		inst->res.sym = node->sym;
		ir_inst_set_oper(CHILD(node, 0), &inst->arg[0]);
		ir_emit_frag(inst);
		break;
	default:
		break;
	}

	return;
}

static void ir_emit_bool_and(struct node_s *node, struct node_s *body_true,
			     struct node_s *body_false)
{
	struct ir_statment_s *inst;
	char *cond_true_label, *cond_false_label, *exit_label;

	cond_true_label = gen_label("t");
	cond_false_label = gen_label("f");
	exit_label = gen_label("e");

	if (!is_node_walked(CHILD(CHILD(node, 0), 0)))
		dag_walk_resume(CHILD(CHILD(node, 0), 0),
				ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 0), 1)))
		dag_walk_resume(CHILD(CHILD(node, 0), 1),
				ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 1), 0)))
		dag_walk_resume(CHILD(CHILD(node, 1), 0),
				ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 1), 1)))
		dag_walk_resume(CHILD(CHILD(node, 1), 1),
				ir_emit_pre,
				ir_emit_post);

	inst = frag_alloc();
	inst->op = ir_invert_cmp_op(dag_to_ir_op(OP(CHILD(node, 0))));
	ir_inst_set_2args(CHILD(node, 0), inst);
	inst->res.type = IR_LABEL;
	inst->res.label = cond_false_label;
	ir_emit_frag(inst);

	inst = frag_alloc();
	inst->op = ir_invert_cmp_op(dag_to_ir_op(OP(CHILD(node, 1))));
	ir_inst_set_2args(CHILD(node, 1), inst);
	inst->res.type = IR_LABEL;
	inst->res.label = cond_false_label;
	ir_emit_frag(inst);

	ir_emit_frag_label(cond_true_label);
	dag_walk_resume(body_true, ir_emit_pre, ir_emit_post);
	ir_emit_frag_goto(exit_label);

	ir_emit_frag_label(cond_false_label);
	dag_walk_resume(body_false, ir_emit_pre, ir_emit_post);
	ir_emit_frag_label(exit_label);
}

static void ir_emit_bool_or(struct node_s *node, struct node_s *body_true,
			    struct node_s *body_false)
{
	struct ir_statment_s *inst;
	char *cond_true_label, *cond_false_label, *exit_label;

	cond_true_label = gen_label("t");
	cond_false_label = gen_label("f");
	exit_label = gen_label("e");

	if (!is_node_walked(CHILD(CHILD(node, 0), 0)))
		dag_walk_resume(CHILD(CHILD(node, 0), 0), ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 0), 1)))
		dag_walk_resume(CHILD(CHILD(node, 0), 1), ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 1), 0)))
		dag_walk_resume(CHILD(CHILD(node, 1), 0), ir_emit_pre,
				ir_emit_post);
	if (!is_node_walked(CHILD(CHILD(node, 1), 1)))
		dag_walk_resume(CHILD(CHILD(node, 1), 1), ir_emit_pre,
				ir_emit_post);

	inst = frag_alloc();
	inst->op = dag_to_ir_op(OP(CHILD(node, 0)));
	ir_inst_set_2args(CHILD(node, 0), inst);
	inst->res.type = IR_LABEL;
	inst->res.label = cond_true_label;
	ir_emit_frag(inst);

	inst = frag_alloc();
	inst->op = dag_to_ir_op(OP(CHILD(node, 1)));
	ir_inst_set_2args(CHILD(node, 1), inst);
	inst->res.type = IR_LABEL;
	inst->res.label = cond_true_label;
	ir_emit_frag(inst);

	ir_emit_frag_label(cond_false_label);
	dag_walk_resume(body_false, ir_emit_pre, ir_emit_post);
	ir_emit_frag_goto(exit_label);

	ir_emit_frag_label(cond_true_label);
	dag_walk_resume(body_true, ir_emit_pre, ir_emit_post);
	ir_emit_frag_label(exit_label);
}

static void ir_emit_bool_cmp(struct node_s *cond, struct node_s *body_true,
			     struct node_s *body_false)
{
	struct ir_statment_s *inst;
	char *cond_true_label, *cond_false_label, *exit_label;

	cond_true_label = gen_label("t");
	cond_false_label = gen_label("f");
	exit_label = gen_label("e");

	if (!is_node_walked(CHILD(cond, 0)))
		dag_walk_resume(CHILD(cond, 0), ir_emit_pre, ir_emit_post);
	if (!is_node_walked(CHILD(cond, 1)))
		dag_walk_resume(CHILD(cond, 1), ir_emit_pre, ir_emit_post);

	inst = frag_alloc();
	inst->op = ir_invert_cmp_op(dag_to_ir_op(OP(cond)));
	ir_inst_set_2args(cond, inst);
	inst->res.type = IR_LABEL;
	inst->res.label = cond_false_label;
	ir_emit_frag(inst);

	ir_emit_frag_label(cond_true_label);
	dag_walk_resume(body_true, ir_emit_pre, ir_emit_post);
	ir_emit_frag_goto(exit_label);
	ir_emit_frag_label(cond_false_label);
	dag_walk_resume(body_false, ir_emit_pre, ir_emit_post);
	ir_emit_frag_label(exit_label);
}

static void ir_emit_if(struct node_s *node)
{
	if (is_op_t_cmp(OP(CHILD(node, 0))))
		ir_emit_bool_cmp(CHILD(node, 0), CHILD(node, 1),
				 CHILD(node, 2));
	else if (OP(CHILD(node, 0)) == K_AND_OP)
		ir_emit_bool_and(CHILD(node, 0), CHILD(node, 1),
				 CHILD(node, 2));
	else if (OP(CHILD(node, 0)) == K_OR_OP)
		ir_emit_bool_or(CHILD(node, 0), CHILD(node, 1),
				CHILD(node, 2));
	else
		ASSERT(false);

	mark_node_walked(CHILD(node, 0));
}

static void ir_emit_while_do(struct node_s *node)
{
	int i;
	struct ir_statment_s *inst;
	struct node_s *cond = CHILD(node, 0);
	char *loop_beg_label = gen_label("l");
	char *loop_exit_label = gen_label("e");

	ir_emit_frag_label(loop_beg_label);

	if (TYPE(cond) == OPR_T)
		for (i = 0; i < cond->op.nops; i++)
			dag_walk_resume(CHILD(cond, i), ir_emit_pre,
					ir_emit_post);

	inst = frag_alloc();
	inst->op = ir_invert_cmp_op(dag_to_ir_op(OP(cond)));
	ir_inst_set_2args(cond, inst);
	inst->res.type = IR_LABEL;
	inst->res.label = loop_exit_label;
	ir_emit_frag(inst);

	dag_walk_resume(CHILD(node, 1), ir_emit_pre, ir_emit_post);

	ir_emit_frag_goto(loop_beg_label);

	ir_emit_frag_label(loop_exit_label);

	mark_node_walked(cond);
}

static void ir_emit_do_while(struct node_s *node)
{
	struct ir_statment_s *inst;
	struct node_s *cond = CHILD(node, 1);
	char *loop_beg_label = gen_label("l");

	ir_emit_frag_label(loop_beg_label);

	dag_walk_resume(CHILD(node, 0), ir_emit_pre, ir_emit_post);

	if (!is_node_walked(CHILD(cond, 0)))
		dag_walk_resume(CHILD(cond, 0), ir_emit_pre, ir_emit_post);
	if (!is_node_walked(CHILD(cond, 1)))
		dag_walk_resume(CHILD(cond, 1), ir_emit_pre, ir_emit_post);

	inst = frag_alloc();
	inst->op = dag_to_ir_op(OP(cond));
	ir_inst_set_2args(cond, inst);
	inst->res.type = IR_LABEL;
	inst->res.label = loop_beg_label;
	ir_emit_frag(inst);

	mark_node_walked(cond);
}

ir_op_t ir_invert_cmp_op(ir_op_t op)
{
	switch ((int)op) {
	case IR_IFGE_OP:
		return IR_IFLEQ_OP;
	case IR_IFLE_OP:
		return IR_IFGEQ_OP;
	case IR_IFLEQ_OP:
		return IR_IFGE_OP;
	case IR_IFGEQ_OP:
		return IR_IFLE_OP;
	case IR_IFEQ_OP:
		return IR_IFNEQ_OP;
	case IR_IFNEQ_OP:
		return IR_IFEQ_OP;
	default:
		return op;
	}
}

ir_op_t ir_reverse_cmp_op(ir_op_t op)
{
	switch ((int)op) {
	case IR_IFGE_OP:
		return IR_IFLE_OP;
	case IR_IFLE_OP:
		return IR_IFGE_OP;
	case IR_IFLEQ_OP:
		return IR_IFGEQ_OP;
	case IR_IFGEQ_OP:
		return IR_IFLEQ_OP;
	default:
		return op;
	}
}

static void ir_print_operands(struct ir_operand_s *o)
{
	switch (o->type) {
	case IR_VOID:
		break;
	case IR_SYM:
		ASSERT(o->sym);
		printf("%s", o->sym->name);
		break;
	case IR_CONST:
		printf("%" PRId64,
		       o->imm.int64);
		break;
	case IR_LABEL:
		ASSERT(o->label);
		printf("%s", o->label);
		break;
	default:
		ASSERT(0);
	}
}

char *gen_label(char *prefix)
{
	static unsigned i;
	char *s = xmalloc(MAX_TMP_STR);

	if (prefix)
		snprintf(s, MAX_TMP_STR, "$L%s_%d", prefix, i++);
	else
		snprintf(s, MAX_TMP_STR, "$L%d", i++);

	return s;
}

static void ir_print_frag(int i, struct ir_statment_s *frag)
{
	int j;
	char *str = alloca(MAX_TMP_STR);

	switch (frag->type) {
	case IR_FRAG_LABEL:
		printf("\t|\t%s:\t|\t\t|\t\t|\t\t|\t\t|\n", frag->label);
		break;
	case IR_FRAG_STR:
		printf("\t%s\n", frag->str);
		break;
	default:
		sprint_ir_op_t(str, frag->op);
		printf("%d\t|\t", i);
		printf("\t|\t");
		printf("%s\t|\t", str);
		ir_print_operands(&frag->res);
		for (j = 0; j < 2; j++) {
			printf("\t|\t");
			ir_print_operands(&frag->arg[j]);
		}
		printf("\t|\n");
	}
}

void ir_dump(void)
{
	unsigned i;
	struct ir_statment_s *frag;
	const char header[] = "\nIR content\n"
		"----------------------------------------------------------------------------------------\n"
		"inst n	|	label	|	opr	|	res	|	arg1	|	arg2	|\n"
		"----------------------------------------------------------------------------------------";

	puts(header);

	frag = ir_code.first;
	i = 0;

	while (1) {
		ir_print_frag(i, frag);

		frag = frag->next;

		if (!frag)
			break;

		if (frag->type == IR_FRAG_INST)
			i++;
	}

	putchar('\n');
}

void ir_free(void)
{
	struct ir_statment_s *frag;
	struct ir_statment_s *tmp;

	frag = ir_code.first;

	while (frag) {
		tmp = frag;
		frag = frag->next;
		if (tmp->type == IR_FRAG_LABEL)
			free(tmp->label);
		else if (tmp->type == IR_FRAG_STR)
			free(tmp->str);
		free(tmp);
	}
}

void dag_to_ir(void)
{
	ir_emit_frag_str(".section .data");
	ir_emit_static_section();
	ir_emit_frag_str(".section .text");
	dag_walk(ir_emit_pre, ir_emit_post);
}
