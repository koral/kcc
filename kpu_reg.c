/* KPU register allocator  */

#include "kpu_reg.h"
#include "mem.h"

#define INVALID_REG	-1

static char __par_tmp_n = REG_TMP_FIRST; /*current number of param/ temps */

static char __par_push_tmp_n = REG_TMP_FIRST;

struct reg_sym_s {
	struct sym_s *sym;
	struct reg_sym_s *next;
};

/* register descriptor */
struct reg_descriptor_s {
	struct reg_sym_s *reg_sym;
} reg[N_REG];

struct spilled_r_s {
	unsigned n;
	char r[N_REG];
};

static struct spilled_r_s spilled_stack;

static void push_spill(char n)
{
	if (spilled_stack.n == N_REG)
		ASSERT(false);

	spilled_stack.r[spilled_stack.n] = n;
	spilled_stack.n++;
}

static char pop_spill(void)
{
	if (!spilled_stack.n)
		return (char)-1;

	return spilled_stack.r[--spilled_stack.n];
}

static void kpu_clean_reg(int n_reg)
{
	struct reg_sym_s *tmp, *reg_sym;

	reg_sym = reg[n_reg].reg_sym;

	while (reg_sym) {
		ASSERT(reg_sym->sym->__reg == n_reg);
		tmp = reg_sym->next;
		free(reg_sym);
		reg_sym = tmp;
	}
	reg[n_reg].reg_sym = NULL;
}

static unsigned kpu_count_reg_sym(unsigned reg_n)
{
	struct reg_sym_s *reg_sym;
	unsigned i = 0;

	reg_sym = reg[reg_n].reg_sym;

	while (reg_sym) {
		i++;
		reg_sym = reg_sym->next;
	}

	return i;
}

void kpu_add_sym2reg(struct sym_s *sym, int n_reg)
{
	struct reg_sym_s *reg_sym = xcalloc(sizeof(*reg_sym));

	reg_sym->sym = sym;

	if (reg[n_reg].reg_sym) {
		struct reg_sym_s *tmp;

		tmp = reg[n_reg].reg_sym;
		reg[n_reg].reg_sym = reg_sym;
		reg[n_reg].reg_sym->next = tmp;
	} else {
		reg[n_reg].reg_sym = reg_sym;
	}

	sym->__reg = n_reg;
}

void kpu_tmp_add(struct sym_s *sym)
{
	if (is_reg_valid(sym->__reg))
		return;

	kpu_add_sym2reg(sym, __par_tmp_n);
	__par_tmp_n++;
}

char kpu_get_param_reg(unsigned (spill_func)(char n))
{
	if (__par_push_tmp_n > REG_TMP_LAST)
		err_fatal(NULL, "register allocation failed, "
				  "too many parameters");
	if (reg[(int)__par_push_tmp_n].reg_sym) {
		push_spill(__par_push_tmp_n);
		spill_func(__par_push_tmp_n);
	}

	return __par_push_tmp_n++;
}

void kpu_fill_regs(unsigned (fill_func)(char n))
{
	char r;

	while (is_reg_valid(r = pop_spill())) {
		fill_func(r);
	}
}

void kpu_reset_param_reg(void)
{
	__par_push_tmp_n = REG_TMP_FIRST;
}

void kpu_clean_tmp_regs(void)
{
	unsigned i;

	for (i = REG_TMP_LAST; i >= REG_TMP_FIRST; i--)
		kpu_clean_reg(i);
}

char kpu_first_free_tmp_reg(void)
{
	unsigned i;

	for (i = REG_TMP_LAST; i >= REG_TMP_FIRST; i--)
		if (reg[i].reg_sym == NULL)
			return i;

	return (char)-1;
}

unsigned kpu_used_tmp_reg(void)
{
	return REG_TMP_LAST - kpu_first_free_tmp_reg();
}

char kpu_curr_sym_reg(struct sym_s *sym)
{
	return sym->__reg;
}

/* Dragon pag. 538 */
char kpu_get_reg(struct ir_statment_s *ir_frag)
{
	unsigned i;
	char reg;

	ASSERT(ir_frag->res.sym);

	if (is_reg_valid(ir_frag->res.sym->__reg))
		return ir_frag->res.sym->__reg;

	for (i = 0; i < 2; i++) {
		if (ir_frag->arg[i].type == IR_SYM &&
		    is_reg_valid(ir_frag->arg[i].sym->__reg) &&
		    kpu_count_reg_sym(ir_frag->arg[i].sym->__reg) == 1 &&
		    ir_frag->arg[i].death) {
			kpu_add_sym2reg(ir_frag->res.sym,
					ir_frag->arg[i].sym->__reg);
			return ir_frag->arg[i].sym->__reg;
		}
	}

	reg = kpu_first_free_tmp_reg();

	if (is_reg_valid(reg)) {
		kpu_add_sym2reg(ir_frag->res.sym, reg);
		return reg;
	}

	err_fatal(NULL, "register allocation failed");

	return INVALID_REG;
}
