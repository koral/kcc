#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include "kcc.h"
#include "mem.h"
#include "dag.h"
#include "hash.h"

#define SYM_CHECK_VERBOSE	(KCC_DEBUG & 1)

struct dag_cont_s dag;

const char *op_t_str[] = {
	FOREACH_op_t(GENERATE_STRING)
};

const char *type_spec_t_str[] = {
	FOREACH_type_spec_t(GENERATE_STRING)
};

const char *c_type_t_str[] = {
	FOREACH_c_type_t(GENERATE_STRING)
};

hash_tab_t main_dag_hash_t;

hash_tab_t glob_sym_t;

bool l_val;

static bool probe;

/* pointer to the last scope */
struct contex_s *curr_scope;
struct contex_s base_scope;

static unsigned hash_str(char *p)
{
	unsigned acc = 0;
	while (*p) {
		acc += *p;
		p++;
	}

	return acc;
}

/* return the hash for a node */
static unsigned hash_node(void *void_p)
{
	char *end;
	struct node_s *node = void_p;
	unsigned acc = 0;
	char *p = (char *)node;

	if (TYPE(node) == ID_T)
		end = (char *)&node->c_type;
	else
		end = (char *)node + node->__size;
	while (p < end) {
		if (p < (char *)&node->c_type ||
			p >= (char *)&node->constant) {
			/* skip forbidden area */
			acc += *p;
		}
		p++;
	}

	if (TYPE(node) == ID_T)
		acc += hash_str(node->id.name);

	return acc;
}

/* return 0 if nodes are equal or something else otherwise */
static int cmp_node(void *p1, void *p2)
{
	struct node_s *n1 = p1;
	struct node_s *n2 = p2;

	if (TYPE(n1) == ID_T && TYPE(n2) == ID_T)
		return	(n1->invalid != n2->invalid) +
			abs(strcmp(n1->id.name, n2->id.name));
	else
		return memcmp(n1, n2, (char *)&n1->c_type - (char *)n1) ||
			memcmp(&n1->constant, &n2->constant,
			       ((char *)n1 + n1->__size) - (char *)&n1->constant);
}


/* hash a symbol */
static unsigned hash_sym(void *void_p)
{
	return hash_str(((struct sym_s *)void_p)->name);
}

/* compare a symbol */
static int cmp_sym(void *p1, void *p2)
{
	return strcmp(((struct sym_s *)p1)->name, ((struct sym_s *)p2)->name);
}

/* free a symbol */
static void free_sym(void *p)
{
	struct sym_s *s = p;

	XFREE(s);
}

static void scope_stack_init(void)
{
	base_scope.curr = glob_sym_t;
	base_scope.prev = NULL;
	curr_scope = &base_scope;
}

static void scope_push(struct node_s *node)
{
	node->op.scope.prev = curr_scope;
	if (!node->op.scope.curr)
		node->op.scope.curr =
			hash_t_create(hash_sym, cmp_sym, free_sym);
	curr_scope = &node->op.scope;
}


static void scope_pop(void)
{
	ASSERT(curr_scope->prev);
	curr_scope = curr_scope->prev;
}

static void dag_walk_rec(struct node_s *node,
			 void (*f_pre)(struct node_s *),
			 void (*f_post)(struct node_s *))
{
	unsigned i;

	if (!node)
		return;
	if (is_node_walked(node))
		return;
	if (node->src_line > curr_loc.l)
		curr_loc.l = node->src_line;

	if (f_pre)
		f_pre(node);

	if (TYPE(node) == OPR_T) {
	    if (OP(node) == K_FUNC_DECL_OP)
		    scope_push(node);
	    if (OP(node) == '=')
		    l_val = true;
	}

	if (TYPE(node) == OPR_T)
		for (i = 0; i < node->op.nops; i++) {
			dag_walk_rec(CHILD(node, i), f_pre, f_post);
			l_val = false;
		}

	if (is_node_walked(node))
		return;

	if (f_post)
		f_post(node);

	if (TYPE(node) == OPR_T &&
	    OP(node) == K_FUNC_DECL_OP)
		scope_pop();

	mark_node_walked(node);
}

/* invalidate a node and all its parents */
static void invalidate_node(struct node_s *node);

static void invalidate_parents(void *arg1, void *arg2)
{
	unsigned i;

	struct node_s *p, *node;

	p = arg1;
	node = arg2;

	if (TYPE(p) == OPR_T)
		for (i = 0; i < p->op.nops; i++)
			if (CHILD(p ,i) == node)
				invalidate_node(p);
}

static void invalidate_node(struct node_s *node)
{
	node->invalid = true;

	h_walk(main_dag_hash_t, invalidate_parents, (void *)node);
}

/* free a single node */
void free_node(void *p)
{
	struct node_s *node = p;
	if (TYPE(node) == ID_T)
		XFREE(node->id.name);
	if (TYPE(node) == STR_LITT_T)
		XFREE(node->constant.str);
	if (TYPE(node) == OPR_T && node->op.scope.curr)
		hash_t_destroy(node->op.scope.curr);

	XFREE(node);
}

void main_dag_init(void)
{
	main_dag_hash_t = hash_t_create(hash_node, cmp_node, free_node);
	glob_sym_t = hash_t_create(hash_sym, cmp_sym, free_sym);
}

void main_dag_free(void)
{
	hash_t_destroy(main_dag_hash_t);
	dag.size = dag.max_size = 0;
	XFREE(dag.dag);
}

struct node_s *mk_leaf_const(uint64_t n, c_type_t t, unsigned line)
{
	struct node_s *p;

	if (KCC_DEBUG & DBG_GEN_DAG)
		printf("kcc %s() at %s:%d\n", __FUNCTION__, __FILE__,
		       __LINE__);

	p = xcalloc(sizeof(struct node_s));

	p->__size = sizeof(*p);
	SET_TYPE(p, CONST_T);
	p->src_line = line;

	p->constant.u_int64 = n;
	p->c_type.t = t;

	return h_add(main_dag_hash_t, p);
}

struct node_s *mk_leaf_const_str(char *str, unsigned line)
{
	struct node_s *p;
	char *s_end;
	unsigned x_n = 0;
	unsigned l_n = 0;
	unsigned u_n = 0;
	char *s = str;

	if (KCC_DEBUG & DBG_GEN_DAG)
		printf("kcc %s() at %s:%d\n", __FUNCTION__, __FILE__,
		       __LINE__);

	p = xcalloc(sizeof(struct node_s));

	for ( ; *s; ++s) {
		*s = tolower((unsigned char)*s);
		if (*s == 'l')
			l_n++;
		else if (*s == 'u')
			u_n++;
		else if (*s == 'x')
			x_n++;

	}

	p->__size = sizeof(*p);
	SET_TYPE(p, CONST_T);
	p->src_line = line;

	if (u_n || x_n) {
		if (x_n)
			p->constant.u_int64 = strtoull(str, &s_end, 16);
		else
			p->constant.u_int64 = strtoull(str, &s_end, 10);
		if (l_n == 2) {
			p->c_type.t = U_LONG_LONG_T;
		}
		else if (l_n == 1) {
			/* FIXME: check for overflow */
			p->c_type.t = U_LONG_T;
		}
		else {
			/* FIXME: check for overflow */
			p->c_type.t = U_INT_T;
		}
	}
	else {
		p->constant.int64 = atoll(str);
		if (l_n == 2) {
			p->c_type.t = LONG_LONG_T;
		}
		else if (l_n == 1) {
			/* FIXME: check for overflow */
			p->c_type.t = LONG_T;
		}
		else {
			/* FIXME: check for overflow */
			p->c_type.t = INT_T;
		}
	}

	XFREE(str);

	return h_add(main_dag_hash_t, p);
}

struct node_s *mk_leaf_id(char *const name, unsigned line)
{
	struct node_s *p;

	if (KCC_DEBUG & DBG_GEN_DAG)
		printf("kcc %s() id '%s' at %s:%d\n", __FUNCTION__,
		       name, __FILE__, __LINE__);

	p = xcalloc(sizeof(struct node_s));

	p->__size = sizeof(*p);
	SET_TYPE(p, ID_T);
	p->id.name = name;
	p->src_line = line;
	p->dag_num = dag.size;

	return h_add(main_dag_hash_t, p);
}

struct node_s *mk_leaf_str_litt_id(char *const str, unsigned line)
{
	struct node_s *p;

	if (KCC_DEBUG & DBG_GEN_DAG)
		printf("kcc %s() id '%s' at %s:%d\n", __FUNCTION__,
		       str, __FILE__, __LINE__);

	p = xcalloc(sizeof(struct node_s));

	p->__size = sizeof(*p);
	SET_TYPE(p, STR_LITT_T);
	p->constant.str = str;
	p->src_line = line;
	p->c_type.t = CHAR_T;
	p->c_type.ref_lev = 1;

	return h_add(main_dag_hash_t, p);
}

struct node_s *mk_leaf_type_spec(type_spec_t t, unsigned line)
{
	struct node_s *p;

	if (KCC_DEBUG & DBG_GEN_DAG)
		printf("kcc %s() type specifier '%s' at %s:%d\n", __FUNCTION__,
		       TYPE_SPEC_T_TO_STR(t), __FILE__, __LINE__);

	p = xcalloc(sizeof(struct node_s));

	p->__size = sizeof(*p);
	SET_TYPE(p, TYPE_SPEC_T);
	p->t_spec.type_spec = t;
	p->src_line = line;

	switch (t) {
	case K_BOOL_T:
		p->c_type.t = BOOL_T;
		break;
	case K_CHAR_T:
		p->c_type.t = CHAR_T;
		break;
	case K_SHORT_T:
		p->c_type.t = SHORT_T;
		break;
	case K_UNSIGNED_T:
	case K_INT_T:
		p->c_type.t = INT_T;
		break;
	case K_LONG_T:
		p->c_type.t = LONG_T;
		break;
	case K_VOID_T:
		p->c_type.t = VOID_T;
		break;
	case K_ENUM_T:
		p->c_type.t = INT_T;
		break;
	case K_STRUCT_T:
		p->c_type.t = STRUCT_T;
		break;
	case K_UNION_T:
		p->c_type.t = UNION_T;
		break;
	case K_POINTER_T:
		p->c_type.ref_lev = 1;
		break;
	default:
		break;
	}

	return h_add(main_dag_hash_t, p);
}

/* all non leaf node are operators */
struct node_s *mk_node(op_t op, unsigned line, bool assign, unsigned nops, ...)
{
	va_list ap;
	int i;
	size_t size;
	struct node_s *p;

	if (KCC_DEBUG & DBG_GEN_DAG) {
		char *os = alloca(MAX_TMP_STR);
		sprint_op_t(os, op);
		printf("kcc %s() op '%s' at %s:%d\n", __FUNCTION__,
			os, __FILE__, __LINE__);
	}
	if (nops)
		size = sizeof(*p) + (nops - 1) * sizeof(struct node_s *);
	else
		size = sizeof(*p);
	p = xcalloc(size);

	p->__size = size;
	SET_TYPE(p, OPR_T);
	SET_OP(p, op);
	p->op.nops = nops;
	p->src_line = line;
	va_start(ap, nops);
	for (i = 0; i < nops; i++)
		SET_CHILD(p, i, va_arg(ap, struct node_s *));
	va_end(ap);

	if (assign)
		invalidate_node(CHILD(p, 0));

	p = h_add(main_dag_hash_t, p);

	return p;
}

void dag_add(struct node_s *node)
{
	unsigned new_size = 0;

	if (!dag.max_size)
		new_size = 256;
	else if (dag.max_size == dag.size)
		new_size = dag.max_size << 2;

	if (new_size) {
		dag.dag = xrealloc(dag.dag, new_size * sizeof(dag.dag));
		dag.max_size = new_size;
	}

	dag.dag[dag.size++] = node;
}

void dag_walk_resume(struct node_s * node,
		     void (*f_pre)(struct node_s *),
		     void (*f_post)(struct node_s *))
{
	dag_walk_rec(node, f_pre, f_post);
}

void dag_walk(void (*f_pre)(struct node_s *), void (*f_post)(struct node_s *))
{
	unsigned i;

	curr_loc.l = 0;
	scope_stack_init();
	probe = dag.dag[0]->__walked;

	for (i = 0; i < dag.size; i++)
		dag_walk_rec(dag.dag[i], f_pre, f_post);
}

struct sym_s *sym_add_curr_scope(char *name, storage_class_t st_class,
				 c_type_t t, unsigned ref_lev,
				 orig_t orig, unsigned n_elm,
				 struct const_s *val)
{
	struct c_type_s c_type;
	struct sym_s *sym = xmalloc(sizeof(*sym));

	c_type.t = t;
	c_type.ref_lev = ref_lev;
	c_type.orig = orig;

	ASSERT(curr_scope);

	if (SYM_CHECK_VERBOSE) {
		char *p = alloca(MAX_TMP_STR);
		sprint_ctype_s(p, &c_type);
		printf("kcc %s() add sym '%s' of type %s to %p at %s:%d\n",
			__FUNCTION__, name, p, curr_scope->curr, __FILE__,
			__LINE__);
	}

	sym->c_type = c_type;
	sym->name = name;
	sym->size = n_elm;
	sym->val = val;
	sym->st_class = st_class;
	sym->__reg = -1;

	return h_add(curr_scope->curr, sym);
}

struct sym_s *sym_resolve(char *name)
{
	struct sym_s *sym;
	struct contex_s *scope = curr_scope;

	do {
		sym = alloca(sizeof(*sym));
		sym->name = name;

		/* FIXME: do not overwrite prev syms */
		sym = h_search(scope->curr, sym);

		if (sym)
			return sym;

		scope = scope->prev;
	} while (scope);

	return NULL;
}

void sym_walk_curr_scope(void (*f)(void *s, void *p))
{
	h_walk(curr_scope->curr, f, NULL);
}

bool is_node_walked(struct node_s *node)
{
	return node->__walked != probe;
}

void mark_node_walked(struct node_s *node)
{
	node->__walked = !probe;
}
