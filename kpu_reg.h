#ifndef __KPU_REG_H__
#define __KPU_REG_H__

#include "ir.h"

#define RET_REG	0
#define N_REG 32

#define ZEROED_REG 25		/* we assume this reg to be 0*/
#define FRAME_POINTER 26
#define STACK_POINTER 27
#define RET_ADDR_REG 28

/* parameters / temporaries regs */
#define REG_TMP_FIRST 1
#define REG_TMP_LAST 24

static inline bool is_reg_valid(char r)
{
	return r>= 0 && r < N_REG;
}

/* allocate a register to store the result of an ir instruction */
char kpu_get_reg(struct ir_statment_s *ir_frag);

/* allocate register to push a parameter to */
char kpu_get_param_reg(unsigned (spill_func)(char n));

/* fill register spilled before function call */
void kpu_fill_regs(unsigned (fill_func)(char n));

/* find the first tmp register free */
char kpu_first_free_tmp_reg(void);

/* number of used temporaries */
unsigned kpu_used_tmp_reg(void);

char kpu_curr_sym_reg(struct sym_s *sym);

void kpu_add_sym2reg(struct sym_s *sym, int n_reg);

void kpu_reset_param_reg(void);

void kpu_tmp_add(struct sym_s *sym);

void kpu_clean_tmp_regs(void);

#endif
