/* original printsource code Frank Thomas Braun */

/* Generation of the graph of the syntax dag */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "kcc.h"
#include "parser.h"
#include "dag.h"

#define POINTER_VERBOSE		1	/* add to the dag graph
					   node addresses */
#define TYPE_VERBOSE		1	/* add to the dag graph
					   node C type */

#define lmax 500
#define cmax 500

int del = 1; /* distance of graph columns */
int eps = 3; /* distance of graph lines */

char graph[lmax][cmax]; /* array for ASCII-Graphic */
unsigned graph_number;

static void graph_init(void);
static void graph_finish();
static void graph_box(char *s, int *w, int *h);
static void graph_draw_box(char *s, int c, int l);
static void graph_draw_arrow(int c1, int l1, int c2, int l2);

/* recursive drawing of the syntax dag */
static void draw_rec(struct node_s *p, int c, int l, int *ce, int *cm);

/*****************************************************************************/


/* draw all dags */
void dags_draw_all(void)
{
	unsigned i;

        graph_number = 0;

	for (i = 0; i < dag.size; i++)
		dag_draw(dag.dag[i]);
}

/* draw a single dag */
void dag_draw(struct node_s *p)
{
	int rte, rtm;

	graph_init();
	draw_rec(p, 0, 0, &rte, &rtm);
	graph_finish();

	return;
}


static void draw_rec(struct node_s *p,
		     int c, int l,	     /* start column and line of node */
		     int *ce, int *cm)    /* resulting end column and mid of node */
{
	int w, h;	    /* node width and height */
	char *s;	    /* node text */
	int cbar;	    /* "real" start column of node (centred above subnodes) */
	int k;		    /* child number */
	int che, chm;	    /* end column and mid of children */
	int cs;		    /* start column of children */
	char word[64];	    /* extended node text */
	char *c_p= word;
	char *os;

	strncpy(word, "???", sizeof(word)); /* should never appear */
	s = word;

	if (!p) {
		sprintf(word, "NULL");
	}
	else {
		switch(TYPE(p)) {
		case TYPE_SPEC_T:
			c_p += snprintf(word, sizeof(word), "type_spec(%s)",
					TYPE_SPEC_T_TO_STR(p->t_spec.type_spec));
			break;
		case CONST_T:
			c_p += snprintf(word, sizeof(word), "const(%d)",
					p->constant.int32);
			break;
		case ID_T:
			c_p += snprintf(word, sizeof(word), "id(%s)",
					p->id.name);
			break;
		case OPR_T:
			os = alloca(MAX_TMP_STR);
			sprint_op_t(os, OP(p));
			c_p += snprintf(s, sizeof(word), "[%s]", os);
			break;
                case STR_LITT_T:
			c_p += snprintf(word, sizeof(word), "str_litt(%s)",
					p->constant.str);
			break;
		}
		if (TYPE_VERBOSE) {
			*c_p++ = '(';
			c_p = sprint_ctype_s(c_p, &p->c_type);
			*c_p++ = ')';
		}
		if (POINTER_VERBOSE)
			c_p += snprintf(c_p, sizeof(word) - (c_p - word),
					"(%p)", p);
	}

/* construct node text box */
	graph_box(s, &w, &h);
	cbar = c;
	*ce = c + w;
	*cm = c + w / 2;

/* node is leaf */
	if (!p	|| TYPE(p) == CONST_T || TYPE(p) == ID_T ||
		p->op.nops == 0) {
		graph_draw_box(s, cbar, l);
		return;
	}

/* node has children */
	cs = c;
	ASSERT(TYPE(p) == OPR_T);
	for (k = 0; k < p->op.nops; k++) {
		draw_rec(CHILD(p, k), cs, l+h+eps, &che, &chm);
		cs = che;
	}

/* total node width */
	if (w < che - c) {
		cbar += (che - c - w) / 2;
		*ce = che;
		*cm = (c + che) / 2;
	}

/* draw node */
	graph_draw_box(s, cbar, l);

/* draw arrows (not optimal: children are drawn a second time) */
	cs = c;
	for (k = 0; k < p->op.nops; k++) {
		draw_rec(CHILD(p, k), cs, l+h+eps, &che, &chm);
		graph_draw_arrow(*cm, l+h, chm, l+h+eps-1);
		cs = che;
	}
}

/* interface for drawing */

static void graph_test(int l, int c)
{
	int ok;

	ok = 1;
	if (l < 0)
		ok = 0;
	if (l >= lmax)
		ok = 0;
	if (c < 0)
		ok = 0;
	if (c >= cmax)
		ok = 0;
	if (ok)
		return;
	printf("\n+++error: l=%d, c=%d not in drawing rectangle 0, 0 ... %d, %d",
	       l, c, lmax, cmax);
	exit(1);
}

static void graph_init(void)
{
	int i, j;

	for (i = 0; i < lmax; i++) {
		for (j = 0; j < cmax; j++) {
			graph[i][j] = ' ';
		}
	}
}

static void graph_finish()
{
	int i, j;

	for (i = 0; i < lmax; i++) {
		for (j = cmax-1; j > 0 && graph[i][j] == ' '; j--);
		graph[i][cmax-1] = 0;
		if (j < cmax-1)
			graph[i][j+1] = 0;
		if (graph[i][j] == ' ')
			graph[i][j] = 0;
	}
	for (i = lmax-1; i > 0 && graph[i][0] == 0; i--)
		;
	printf("\n\nGraph %d:\n", graph_number++);
	for (j = 0; j <= i; j++)
		printf("\n%s", graph[j]);
	printf("\n");
}

static void graph_box(char *s, int *w, int *h)
{
	*w = strlen(s) + del;
	*h = 1;
}

static void graph_draw_box(char *s, int c, int l)
{
	int i;

	graph_test(l, c+strlen(s)-1+del);
	for (i = 0; i < strlen(s); i++) {
		graph[l][c+i+del] = s[i];
	}
}

static void graph_draw_arrow(int c1, int l1, int c2, int l2)
{
	int m;

	graph_test(l1, c1);
	graph_test(l2, c2);
	m = (l1 + l2) / 2;
	while (l1 != m) {
		graph[l1][c1] = '|';
		if (l1 < l2)
			l1++;
		else l1--;
	}
	while (c1 != c2) {
		graph[l1][c1] = '-';
		if (c1 < c2) c1++;
		else c1--;
	}
	while (l1 != l2) {
		graph[l1][c1] = '|';
		if (l1 < l2) l1++;
		else l1--;
	}
	graph[l1][c1] = '|';
}
