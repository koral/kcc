#ifndef __MEM_H__
#define __MEM_H__


#include <stdlib.h>
#include <string.h>
#include "err.h"  /* Needed for yyerror() */

#define XFREE(p)				\
	do {					\
		if (p) {			\
			free(p);		\
			p = NULL;		\
		}				\
	} while(0)

static inline void *xmalloc(size_t size)
{
	void *p = malloc(size);

	if (!p)
		err_fatal(NULL, "out of memory");

	return p;
}

static inline void *xcalloc(size_t size)
{
	void *p = calloc(1, size);

	if (!p)
		err_fatal(NULL, "out of memory");

	return p;
}

static inline void *xrealloc(void *p, size_t size)
{
	p = realloc(p, size);

	if (!p)
		err_fatal(NULL, "out of memory");

	return p;
}

static inline char *xstrdup(const char *s)
{
	return strndup(s, MAX_TMP_STR);
}

#endif
