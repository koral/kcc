#include <alloca.h>
#include "kcc.h"
#include "t_check.h"
#include "mem.h"

static void type_propagate_unary_op(struct node_s *node)
{
	ASSERT(node->op.nops == 1);

	switch ((int)OP(node)) {
	case K_DE_REF_OP:
		if (!CHILD(node, 0)->c_type.ref_lev)
			err_fatal(NULL, "Invalid derefernce");
		node->c_type = CHILD(node, 0)->c_type;
		node->c_type.ref_lev--;
		return;
	case K_REF_OP:
		/* FIXME: cannot reference more then once */
		node->c_type = CHILD(node, 0)->c_type;
		node->c_type.ref_lev++;
		return;
	case '~':
		node->c_type = CHILD(node, 0)->c_type;
		return;
	case '!':
		node->c_type.t = INT_T;
		node->c_type.ref_lev = 0;
		return;
	}
}

/* check operands type */
static void type_check_bin_op(op_t op, struct c_type_s *t1, struct c_type_s *t2)
{
	unsigned op_n;

	switch ((int)op) {
	case K_EXT_DECLARATION_OP:
	case K_DECLARATION_OP:
	case ';':
		return;
	}
	switch ((int)op) {
	case '/':
	case '*':
	case '%':
		if (t1->ref_lev || t2->ref_lev)
			err_fatal(NULL, "Usupported pointer arithmetic");
	case '+':
		if (t1->ref_lev && t2->ref_lev)
			err_fatal(NULL, "Usupported pointer arithmetic, "
				  "cannot sum two pointers");
	case '=':
	case '-':
		if (t1->t == VOID_T || t1->t > U_LONG_LONG_T) {
			op_n = 1;
			goto op1_error;
		}
		if (t2->t == VOID_T || t2->t > U_LONG_LONG_T) {
			op_n = 2;
			goto op1_error;
		}
	default:
		return;
	}
op1_error:
	{
		char *ct = alloca(MAX_TMP_STR);
		char *os = alloca(MAX_TMP_STR);
		sprint_ctype_s(ct, t1);
		sprint_op_t(os, op);
		err_fatal(NULL, "Usupported operand number %d of type "
			"%s for operator %s", op_n, ct, os);
		return;
	}
}

static void type_propagate_bin_op(struct node_s *node)
{
	signedness_t s;
	c_type_t ret_t;
	struct c_type_s *t0, *t1;

	ASSERT(node->op.nops == 2);

	t0 = &CHILD(node, 0)->c_type;
	t1 = &CHILD(node, 1)->c_type;

	type_check_bin_op(OP(node),
			  &CHILD(node, 0)->c_type,
			  &CHILD(node, 1)->c_type);

	switch((int)OP(node)) {
	case '+':
	case '-':
		if (t0->ref_lev != t1->ref_lev)
			node->c_type.ref_lev = MAX(t0->ref_lev, t1->ref_lev);
	case '=':
	case '/':
	case '*':
	case '%':
		if (signedness(t0->t) == UNSIGNED_T ||
		    signedness(t1->t) == UNSIGNED_T)
			s = UNSIGNED_T;
		else
			s = SIGNED_T;
		if (MAX(t0->t, t1->t) <= U_INT_T)
			ret_t = INT_T;
		else
			ret_t = MAX(t0->t, t1->t);
		if (signedness(ret_t) != s)
			ret_t++;
		node->c_type.t = ret_t;
		return;
	case '<':
	case '>':
	case K_LE_OP:
	case K_GE_OP:
	case K_NE_OP:
	case K_AND_OP:
	case K_OR_OP:
	case K_EQ_OP:
		node->c_type.t = INT_T;
		return;
	case K_POSTFIX_OP:
		node->c_type = CHILD(node, 0)->c_type;
	default:
		return;
	}
}

/* ret 1 if the node is a valid expression statement 0 otherwise */
static int is_expr_statement(struct node_s *node)
{
	if (!node->c_type.ref_lev &&
	    (node->c_type.t == VOID_T ||
	     node->c_type.t > U_LONG_LONG_T))
		return 0;

	return 1;
}

/* a declaration add the id to the symbol table */
static void declare_sym_var(struct node_s *node)
{
	storage_class_t st_class;

	switch (OP(node)) {
	case K_DECLARATION_OP:
		st_class = AUTO_T;
		break;
	case K_EXT_DECLARATION_OP:
		st_class = EXT_T;
		break;
	case K_PARAM_DECL_OP:
		st_class = REG_T;
		break;
	default:
		ASSERT(0);
		break;
	}

	switch ((int)TYPE(CHILD(node, 1))) {
	case ID_T:
		CHILD(node, 1)->sym =
			sym_add_curr_scope(CHILD(node, 1)->id.name,
					   st_class,
					   CHILD(node, 0)->c_type.t,
					   CHILD(node, 1)->c_type.ref_lev,
					   VAR_O, 1, NULL);
		break;
	case OPR_T:
		if (OP(CHILD(node, 1)) == K_ARRAY_DECL_OP) {
			CHILD(CHILD(node, 1), 0)->sym =
				sym_add_curr_scope(CHILD(CHILD(node, 1), 0)->id.name,
						   st_class,
						   CHILD(node, 0)->c_type.t,
						   CHILD(node, 1)->c_type.ref_lev + 1,
						   ARR_O,
						   CHILD(CHILD(node, 1), 1)->constant.u_int64,
						   NULL);
		} else {
			ASSERT(OP(CHILD(node, 1)) == '=');
			ASSERT(TYPE(CHILD(CHILD(node, 1), 0)) == ID_T);
			CHILD(node, 1)->sym =
				sym_add_curr_scope(CHILD(CHILD(node, 1), 0)->id.name,
						   st_class,
						   CHILD(node, 0)->c_type.t,
						   CHILD(CHILD(node, 1), 0)->c_type.ref_lev,
						   VAR_O, 1,
						   &CHILD(CHILD(node, 1), 1)->constant);
		}
		break;
	default:
		ASSERT(false);
		break;
	}

}

static void declare_sym_func(struct node_s *node)
{
	CHILD(node, 1)->sym = sym_add_curr_scope(CHILD(node, 1)->id.name,
						EXT_T,
						CHILD(node, 0)->c_type.t,
						CHILD(node, 0)->c_type.ref_lev,
						FUNC_O, 1, NULL);
}

static void t_prop(struct node_s *node)
{
	struct sym_s *sym;

	node->l_val = l_val;

	if (TYPE(node) == OPR_T) {
		switch ((int)OP(node)) {
		case K_FUNC_DECL_OP:
			declare_sym_func(node);
			break;
		break;
		case K_DECLARATION_OP:
		case K_EXT_DECLARATION_OP:
		case K_PARAM_DECL_OP:
			declare_sym_var(node);
			break;
		}
	}
	else if (TYPE(node) == ID_T) {
		/* FIXME: do not overwrite prev syms */
		sym = sym_resolve(node->id.name);
		if (!sym)
			err_fatal(NULL, "Symbol \"%s\" undefined",
				  node->id.name);
		node->c_type = sym->c_type;
		node->sym = sym;
	}

	return;
}

static void t_check(struct node_s *node)
{
	unsigned op_n;

	if (TYPE(node) == OPR_T) {

		if (OP(node) == K_FUNC_CALL_OP) {
			node->c_type = CHILD(node, 0)->c_type;
		} else  if (OP(node) == K_IF_ELSE_OP ||
			    OP(node) == K_WHILE_OP) {
			if (!is_expr_statement(CHILD(node, 0))) {
				op_n = 1;
				goto err;
			}
		}
		else if (OP(node) == K_DO_WHILE_OP) {
			if (!is_expr_statement(CHILD(node, 1))) {
				op_n = 2;
				goto err;
			}
		}
		else if (node->op.nops == 1) {
			type_propagate_unary_op(node);
		}
		else if (node->op.nops == 2 &&
			 CHILD(node, 0) && CHILD(node, 1)) {
			type_propagate_bin_op(node);
		}
	}

	return;
err:
	{
		char *ct = alloca(MAX_TMP_STR);
		char *os = alloca(MAX_TMP_STR);
		sprint_ctype_s(ct, &CHILD(node, op_n - 1)->c_type);
		sprint_op_t(os, OP(node));
		err_fatal(NULL, "Usupported operand number %d of type "
			  "%s for operator %s", op_n, ct, os);
		return;
	}
}

void type_prop_check(void)
{
    dag_walk(t_prop, t_check);
}
