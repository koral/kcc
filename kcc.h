#ifndef __KCC_H__
#define __KCC_H__

#include <assert.h>

#define ASM_GEN()		kpu_asm_gen()
#define ASM_PEEPHOLES()		kpu_asm_peepholes()
#define ASM_DUMP()		kpu_asm_dump()
#define ASM_FREE()		kpu_asm_free()

#define DBG_PARSER		(1 << 0)
#define DBG_GEN_DAG		(1 << 1)
#define DBG_DRAW_DAG		(1 << 2)
#define DBG_DUMP_HASH		(1 << 3)
#define DBG_DUMP_IR		(1 << 4)

#ifndef KCC_DEBUG
#define KCC_DEBUG					\
    (							\
	DBG_PARSER |					\
	DBG_GEN_DAG |					\
	DBG_DRAW_DAG |					\
	DBG_DUMP_HASH |					\
	DBG_DUMP_IR)
#endif

#define ASSERTIONS	1	/* enable assertions */

#if ASSERTIONS
#define ASSERT(x)	assert(x)
#else
#define ASSERT(x)
#endif

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

#define MAX(a, b)	((a) > (b) ? (a) : (b))
#define MIN(a, b)	((a) > (b) ? (b) : (a))

#define MAX_LIT_LEN	512	/* Max literal string length */
#define MAX_TMP_STR	256
#define MAX_IF_NESTING	256

typedef char bool;

enum { false, true };

struct loc_s {
	char *f;	/* filename */
	unsigned l;	/* line number */
};

extern struct loc_s curr_loc;

#endif
