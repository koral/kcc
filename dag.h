#ifndef __DAG_H__
#define __DAG_H__

#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "kcc.h"
#include "parser.h"
#include "hash.h"

typedef enum {
	AUTO_T,		/* automatic */
	REG_T,		/* register */
	EXT_T,		/* external */
	STATIC_T	/* static */
} storage_class_t;

/* node types */
typedef enum { CONST_T,		/* constant */
	       ID_T,		/* identifier (symbol) */
	       TYPE_SPEC_T,	/* type specifier */
	       STR_LITT_T,	/* litteral string */
	       OPR_T,		/* operator */
} node_t;

/* operator types */
#define FOREACH_op_t(op_t)			\
	op_t(K_INIT_OP = 256)			\
	op_t(K_INC_OP)				\
	op_t(K_DEC_OP)				\
	op_t(K_LEFT_OP)				\
	op_t(K_RIGHT_OP)			\
	op_t(K_LE_OP)				\
	op_t(K_GE_OP)				\
	op_t(K_EQ_OP)				\
	op_t(K_NE_OP)				\
	op_t(K_AND_OP)				\
	op_t(K_OR_OP)				\
	op_t(K_MUL_ASSIGN_OP)			\
	op_t(K_DIV_ASSIGN_OP)			\
	op_t(K_MOD_ASSIGN_OP)			\
	op_t(K_ADD_ASSIGN_OP)			\
	op_t(K_SUB_ASSIGN_OP)			\
	op_t(K_LEFT_ASSIGN_OP)			\
	op_t(K_RIGHT_ASSIGN_OP)			\
	op_t(K_AND_ASSIGN_OP)			\
	op_t(K_XOR_ASSIGN_OP)			\
	op_t(K_OR_ASSIGN_OP)			\
	op_t(K_POSTFIX_OP)			\
	op_t(K_REF_OP)				\
	op_t(K_DE_REF_OP)			\
	op_t(K_FUNC_DECL_OP)			\
	op_t(K_PARAM_DECL_OP)			\
	op_t(K_DIRECT_DECL_OP)			\
	op_t(K_DECLARATION_OP)			\
	op_t(K_EXT_DECLARATION_OP)		\
	op_t(K_DECL_SPEC_OP)			\
	op_t(K_CODE_BLOCK_OP)			\
	op_t(K_IF_ELSE_OP)			\
	op_t(K_SWITCH_OP)			\
	op_t(K_CASE_OP)				\
	op_t(K_WHILE_OP)			\
	op_t(K_DO_WHILE_OP)			\
	op_t(K_ARRAY_DECL_OP)			\
	op_t(K_RETURN_OP)			\
	op_t(K_FUNC_CALL_OP)			\
	op_t(K_LAST_OP)

typedef enum {
FOREACH_op_t(GENERATE_ENUM)
} op_t;

static inline bool is_op_t_cmp(op_t t)
{
	switch ((int)t) {
	case '>':
	case '<':
	case K_GE_OP:
	case K_LE_OP:
	case K_EQ_OP:
	case K_NE_OP:
		return true;
	default:
		return false;
	}
}

static inline bool is_op_t_cmp_bool(op_t t)
{
	switch ((int)t) {
	case '>':
	case '<':
	case K_GE_OP:
	case K_LE_OP:
	case K_EQ_OP:
	case K_NE_OP:
	case K_AND_OP:
	case K_OR_OP:
		return true;
	default:
		return false;
	}
}

extern const char *op_t_str[];

static inline char *sprint_op_t(char *str,  op_t t)
{
	if (t <= 255) {
		*str++ = t;
	}
	else {
		char const *c = op_t_str[t - K_INIT_OP] + 2;
		while (*c) {
			*str = *c;
			str++;
			c++;
		}
	}
	*str = 0;

	return str;
}

/* type identifiers */
#define FOREACH_type_spec_t(type_spec_t)	\
	type_spec_t(K_INIT_T = K_LAST_OP)	\
	type_spec_t(K_BOOL_T)			\
	type_spec_t(K_CHAR_T)			\
	type_spec_t(K_SHORT_T)			\
	type_spec_t(K_INT_T)			\
	type_spec_t(K_LONG_T)			\
	type_spec_t(K_SIGNED_T)			\
	type_spec_t(K_UNSIGNED_T)		\
	type_spec_t(K_FLOAT_T)			\
	type_spec_t(K_DOUBLE_T)			\
	type_spec_t(K_VOID_T)			\
	type_spec_t(K_POINTER_T)		\
	type_spec_t(K_COMPLEX_T)		\
	type_spec_t(K_IMAGINARY_T)		\
	type_spec_t(K_STRUCT_T)			\
	type_spec_t(K_UNION_T)			\
	type_spec_t(K_ENUM_T)			\
	type_spec_t(K_LAST_T)

typedef enum {
	FOREACH_type_spec_t(GENERATE_ENUM)
} type_spec_t;

extern const char *type_spec_t_str[];

#define TYPE_SPEC_T_TO_STR(x)	(type_spec_t_str[(x) - K_INIT_T] + 2)

/* C types */
#define FOREACH_c_type_t(c_type_t)		\
	c_type_t(VOID_T)			\
	c_type_t(BOOL_T)			\
	c_type_t(CHAR_T)			\
	c_type_t(U_CHAR_T)			\
	c_type_t(SHORT_T)			\
	c_type_t(U_SHORT_T)			\
	c_type_t(INT_T)				\
	c_type_t(U_INT_T)			\
	c_type_t(LONG_T)			\
	c_type_t(U_LONG_T)			\
	c_type_t(LONG_LONG_T)			\
	c_type_t(U_LONG_LONG_T)			\
	c_type_t(STRUCT_T)			\
	c_type_t(UNION_T)

typedef enum {
	FOREACH_c_type_t(GENERATE_ENUM)
} c_type_t;

extern const char *c_type_t_str[];


#define POINTER_SIZE 4

/* size in byte for the varius type defined above */
static const unsigned char _type_size[] = { -1, 1, 1, 1, 2, 2, 4, 4, 4, 4, 8, 8,
                                           -1, -1 };

/* where symbol was originated from */
typedef enum {
	VAR_O,
	TMP_O,
	FUNC_O,
	ARR_O
} orig_t;

struct c_type_s {
	c_type_t t;
	unsigned ref_lev;
	orig_t orig;
};

static inline unsigned type_size(struct c_type_s *t)
{
	if (t->ref_lev)
		return POINTER_SIZE;
	else
		return _type_size[t->t];
}

static inline unsigned type_base_size(struct c_type_s *t)
{
        return _type_size[t->t];
}

static inline char *sprint_ctype_s(char *str, struct c_type_s *t)
{
	int i;
	char const *c = c_type_t_str[t->t];

	for (i = 0; i < t->ref_lev; i++) {
		*str = '*';
		str++;
	}

	while (*c != '_') {
		*str = *c;
		str++;
		c++;
	}
	*str = 0;

	return str;
}

typedef enum {
	NO_SIGNEDNESS_T,
	SIGNED_T,
	UNSIGNED_T
} signedness_t;

/* return 1 if the type is signed or 0 otherwhise */
static inline signedness_t signedness(c_type_t t)
{
	if (t && t < STRUCT_T) {
		if(c_type_t_str[t][0] == 'U' &&
			c_type_t_str[t][1] == '_')
			return UNSIGNED_T;
		else
			return SIGNED_T;
	}
	else
		return NO_SIGNEDNESS_T;
}

/* constant value */
struct const_s {
	union {
		int8_t int8;
		uint8_t u_int8;
		int16_t int16;
		uint16_t u_int16;
		int32_t int32;
		uint32_t u_int32;
		int64_t int64;
		uint64_t u_int64;
	};
	char *str;
};

/* identifier (symbol) */
struct id_s {
	char *name;
};

/* type specifiers */
struct type_spec_s {
	type_spec_t type_spec;
};

/* holds the symbol hash table for the current context and a pointer to the
   upper one */
struct contex_s {
        hash_tab_t *curr;
        struct contex_s *prev;
};

/* hold a symbol table element	*/
struct sym_s {
	char *name;
	struct c_type_s c_type;
	unsigned size;			/* size in elements */
	struct const_s *val;		/* can be used for initialized values or
					   const propagation */
	char __reg;
	storage_class_t st_class;       /* storage class */
	void *ir_last;			/* last ir frag mentioning the symbol */
};

/* non leaf nodes (operators) */
struct op_s {
        struct contex_s scope;		/* in case a new scope is created */
	unsigned nops;			/* number of operands */
	op_t __opr;			/* operator */
	struct node_s *__op[1];		/* operands, extended at runtime */
};

struct node_s {
	size_t __size;				/* node size in byte */
	node_t __node_type;			/* type of node */
	unsigned dag_num;			/* dag number */
	bool invalid;
	bool l_val;				/* true if l-value */
	/* begin of hash forbidden area */
	struct c_type_s c_type;
	unsigned src_line;
	struct sym_s *sym;			/* during ir generation most of
						   nodes gets tmp symbols */
	bool __walked;				/* to avoid double walk */
	/* end of hash forbidden area */
	union {
		struct const_s constant;	/* constants */
		struct id_s id;			/* identifier (symbol) */
		struct op_s op;			/* operators */
		struct type_spec_s t_spec;	/* type specifier */
	};
};

struct dag_cont_s {
	unsigned max_size;	/* max num of dags we can currently store */
	unsigned size;		/* acutal number of dags currently stored */
	struct node_s **dag;	/* array of dags */
};

extern struct dag_cont_s dag;

/* main dag hash table holding all nodes */
extern hash_tab_t main_dag_hash_t;

/* symbol table for global variable */
extern hash_tab_t glob_sym_t;

/* mark if the current evaluated node is l-val */
bool l_val;

/* node type */
#define TYPE(n)			(ASSERT(n), ((n)->__node_type))
#define SET_TYPE(n, t)		(ASSERT(n), ((n)->__node_type = t))
/* node operator (non leaf nodes) */
#define OP(n)			(ASSERT(n),			\
				 ASSERT(TYPE(n) == OPR_T),	\
				 (n)->op.__opr)
#define SET_OP(n, oper)		(ASSERT(n),			\
				 ASSERT(TYPE(n) == OPR_T),	\
				 (n)->op.__opr = (oper))
/* node child */
#define CHILD(n, i)		(ASSERT(n),				\
				 ASSERT(TYPE(n) == OPR_T),		\
				 ASSERT((i) < (n)->op.nops),		\
				 (n)->op.__op[i])
#define SET_CHILD(n, i, p)	(ASSERT(n),			      \
				 ASSERT(TYPE(n) == OPR_T),	      \
				 ASSERT((i) < (n)->op.nops),	      \
				 (n)->op.__op[i] = (p))

/* init the main parse dag */
void main_dag_init(void);
/* free all parse dag stored into the main dag */
void main_dag_free(void);

/* add a dag into our dag container */
void dag_add(struct node_s *node);

/* the functions are use to create leaf or nodes */
struct node_s *mk_leaf_const(uint64_t n, c_type_t t, unsigned line);
struct node_s *mk_leaf_const_str(char *str, unsigned line);
struct node_s *mk_leaf_id(char *const name, unsigned line);
struct node_s *mk_leaf_type_spec(type_spec_t t, unsigned line);
struct node_s *mk_node(op_t op, unsigned line, bool assign, unsigned nops, ...);
struct node_s *mk_leaf_str_litt_id(char *const str, unsigned line);

/* dump a dag to stdout */
void dag_draw(struct node_s *p);
/* dump all dags to stdout */
void dags_draw_all(void);

/* add a symbol to the current scope */
struct sym_s *sym_add_curr_scope(char *name, storage_class_t st_class,
				 c_type_t t, unsigned ref_lev,
				 orig_t orig, unsigned n_elm,
				 struct const_s *val);

/* resolve a symbol using the current scope chain */
struct sym_s *sym_resolve(char *name);

/* walk the symbol table of the current scope */
void sym_walk_curr_scope(void (*f)(void *s, void *p));

/* walk the dag calling f_pre and f_post (if they exists) before and after
   visiting child nodes */
void dag_walk(void (*f_pre)(struct node_s *), void (*f_post)(struct node_s *));

void dag_walk_resume(struct node_s * node,
		     void (*f_pre)(struct node_s *),
		     void (*f_post)(struct node_s *));

bool is_node_walked(struct node_s *node);
void mark_node_walked(struct node_s *node);

#endif
