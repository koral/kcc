#ifndef __T_CHECK_H__
#define __T_CHECK_H__

#include "dag.h"

/* propagate and check types over the dag */
void type_prop_check(void);

#endif
