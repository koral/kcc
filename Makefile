SRCS = parser.c lexer.c kcc.c dag.c dag_draw.c hash.c t_check.c ir.c \
	kpu_gen.c kpu_reg.c tree_optim.c ir_optim.c kpu_peephole.c
OBJS = $(SRCS:.c=.o)
DEPS = $(OBJS:.o=.d)
CC = gcc
override CFLAGS += -O0 -g3 -Wall
LEX = flex
YACC = bison
YACC_OPT = # -t --report=all

%.c: %.y
%.c: %.l

default: kcc

-include $(DEPS)
%.o: %.c
	$(CC) $(CFLAGS) -MMD -c $< -o $@

kcc: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

lexer.c: lexer.l
	$(LEX) lexer.l

parser.c: parser.y lexer.l
	$(YACC) $(YACC_OPT) parser.y

clean:
	rm -rf $(OBJS) $(DEPS) lexer.c lexer.h parser.c parser.h kcc *.i \
	*core* G*

gcc-ir:
	gcc -O0 -g0 -S -fdump-tree-gimple tests/4.c

llvm-ir:
	clang -O0 -S -emit-llvm tests/4.c

lcc-ir:
	$(LCCDIR)/rcc  -target=bytecode tests/4.c
