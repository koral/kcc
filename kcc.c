//-----------------------------------------------------------------------------
// Title	 : KCC main file
// Project	 : KCC
//-----------------------------------------------------------------------------
// File		 : kcc.c
// Author	 : acorallo  <andrea_corallo@yahoo.it>
// Created	 : 11.07.2017
//-----------------------------------------------------------------------------
// Description :
// Main file for kcc compiler
//-----------------------------------------------------------------------------
// This file is part of KPU.
// KPU is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// KPU is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY;
// without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with KPU.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright (c) 2017 by Andrea Corallo.
//------------------------------------------------------------------------------
// Modification history :
// 11.07.2017: created
//-----------------------------------------------------------------------------

#include <stdio.h>
#include "kcc.h"
#include "parser.h"
#include "dag.h"
#include "hash.h"
#include "t_check.h"
#include "tree_optim.h"
#include "ir.h"
#include "ir_optim.h"
#include "kpu_gen.h"
#include "kpu_peephole.h"

extern int yydebug;

struct loc_s curr_loc;

int main(int argc, char* argv[])
{
#if (YYDEBUG)
	yydebug = 1;
#endif
	curr_loc.f = "stdin";

	main_dag_init();

	if (yyparse())
		goto exit_error;

	type_prop_check();

	if (KCC_DEBUG & DBG_DUMP_HASH) {
		printf("\nDag nodes ");
		h_dump(main_dag_hash_t);
	}

	tree_optimizer();

	if (KCC_DEBUG & DBG_DRAW_DAG)
		dags_draw_all();

	dag_to_ir();

	ir_optimizer();

	if (KCC_DEBUG & DBG_DUMP_IR)
		ir_dump();

	ASM_GEN();
	ASM_PEEPHOLES();
	ASM_DUMP();

	main_dag_free();
	ir_free();
	ASM_FREE();

	return 0;
exit_error:

	return 1;
}
