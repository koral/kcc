/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000003

*/

/* pointer access */

int main(void)
{
	int *p;

	p = 0; /* we set the pointer into ram to have write access */
	p[2] = 3;

	return p[2];
}
