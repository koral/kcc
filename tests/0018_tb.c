/*

`EXEC_INSTS(1000)

Reg 0 <= 0x0000000a

*/

/* do-while */

int main(void)
{

	int a = 100;

	do {
		a = a - 1;
	} while (a > 10);

	return a;
}
