/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00001983

*/

/* procedure activation call
   temporary register spill/fill
   prove that an automatic variable is preserved across function calls
 */

int foo(int a)
{
	a = 99;

	return 0;
}

int main(void)
{
	int par0 = 0x1983;

	foo(par0);

	return par0;
}
