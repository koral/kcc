/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000005

*/

/*
  do not remove invalidated common expressions
*/

int main(void)
{
	int i = 3;

	++i;
	++i;

	return i;
}
