/*

`EXEC_INSTS(1000)

Reg 0 <= 0x000000ff

*/

/*
  allocate and initialize a static pointer.
*/

char *p = 0xff;

int main()
{
	return p;
}
