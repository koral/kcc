/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000003

*/

/*
  pre decrement increment operators
*/

int main(void)
{
	int i = 3;

	--i;

	return ++i;
}
