/*

`EXEC_INSTS(1000)

Reg 0 <= 0x0000000a

*/

/* while */

int main(void)
{
	int a = 0;

	while (a < 10) {
		a = a + 1;
	}

	return a;
}
