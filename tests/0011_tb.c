/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000000

*/

/* some boolean logic */

int main(void)
{
	int a = 10;
	int b = 1;

	if (a > 1 && b)
		a = 0;
	else
		a = 1;

	return a;
}
