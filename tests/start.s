;;; Trampoline ROM code for kcc testsuite
_ram:
#pragma offset 0xffff8000
_rom:
	;; Copy the payload into ram
	MOVI	R1, _payload
	MOVI	R2, _ram
_move_data:
	LDW	R0, R1, 0
	STW	R0, R2, 0
	ADDI	R1, R1, 0x4
	ADDI	R2, R2, 0x4
	CMPI	R2, 256         ; Test code can be up to 64 words
	BNEQ	_move_data
	NOP
	;; Set the sp, bp + return pointer and jump into main
_start:
	MOVI	SP, (0x1000 - 4)
	MOV	FP, SP
	JMP	_ram
	MOVI	R28, _death_loop
_death_loop:
	JMP	_death_loop
	NOP
_payload:
