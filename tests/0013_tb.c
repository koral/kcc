/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000001

*/

/* some boolean logic */

int a = 1;
int b = 1;

int main(void)
{
	if (a > 1 && b)
		a = 0;
	else
		a = 1;

	return a;
}
