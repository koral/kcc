/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000001

*/

/* some boolean logic */

int main(void)
{
	int a = 4;
	int b = 1;

	if (a > 10 || !b)
		a = 8;
	else
		a = 1;

	return a;
}
