/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000008

*/

/* procedure activation call  */

int divider(int a, int b)
{
	return a / b;
}

int main(void)
{
	int res;
	int par0 = 16;
	int par1 = 2;

	res = divider(par0, par1);

	return res;
}
