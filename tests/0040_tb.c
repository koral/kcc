/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000001

*/

/*
  Don't mess around with parameter scopes.
*/

int c = 1;

void print_char(int c)
{
        c = 4;
}

int main(void)
{
	return c;
}
