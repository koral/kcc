/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000003

*/

/*
  This testcase was generating wrong assembly due having the load generated in
  the body + liveness analysis was failing to recognize the loop.
*/


char *str = "123";


int main(void)
{
	char *p = str;
	int i = 0;

	while (*p) {
		++i;
		++p;
		*p;
	}

	return i;
}
