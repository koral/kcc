/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000002

*/

/*
  procedure activation call
  check that callee preverve caller regs spilling correctly
*/

int foo(int i)
{
	int a = 0x20;
	int b = 0x10;

	return a + b;
}

int  main()
{
	int var1 = 1;
	int var2 = 2;

	foo(var1);

	return var2;
}
