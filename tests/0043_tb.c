/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000004

*/

/*
  Store a static variables.
*/

int a = 2;

void foo(void)
{
	a = 4;
}

int main(void)
{
	foo();

	return a;
}
