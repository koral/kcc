/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00001983

*/

/*
   testcase 0
   set and return a integer variable
*/

int main(void)
{
	int a;

	a = 0x1983;

	return a;
}
