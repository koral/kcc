/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000021

*/

/*
  send a classic hello world on uart
*/

char *c = "Hello world!";

int main(void)
{
	int i = 12;
        unsigned int *uart_reset = 0xffffffec;
	unsigned int *uart_out = 0xffffffd8;
	char *p = c;

        *uart_reset = 0x0;

	while (i) {
		*uart_out = *p;
		i = i - 1;
		p = p + 1;
	}

	return *(p - 1);
}
