/*

`EXEC_INSTS(1000)

Reg 0 <= 0x000002d0

*/

/* procedure activation call
   compute recursively the factorial of 6
*/

int fact(unsigned int i)
{
	if(i <= 1)
		return 1;

	return i * fact(i - 1);
}

int  main()
{
	return fact(6);
}
