/*

`EXEC_INSTS(1000)

Reg 0 <= 0x00000004

*/

/*
  Some scope testing.
*/

int i = 3;

int foo(int i)
{
	return i;
}

int main(void)
{
	return foo(4);
}
