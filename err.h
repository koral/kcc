#ifndef __ERR_H__
#define __ERR_H__

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "kcc.h"
#include "parser.h"

static inline void err_fatal(struct loc_s *loc, const char *format, ...)
{
	va_list args;

    	fflush(stdout);

	fprintf(stderr, "Error: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	if (!loc)
		loc = &curr_loc;
	fprintf(stderr, ". at %s:%d\n", loc->f, loc->l);

	fflush(stderr);

	exit(1);
}

static inline void err_internal_fatal(struct loc_s *loc, const char *format, ...)
{
	va_list args;

    	fflush(stdout);

	fprintf(stderr, "Internal error: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	if (!loc)
		loc = &curr_loc;
	fprintf(stderr, ". Generate from source at %s:%d\n", loc->f, loc->l);

	fflush(stderr);

	ASSERT(false);
}


static inline void err_warning(struct loc_s *loc, const char *format, ...)
{
	va_list args;

    	fflush(stdout);

	fprintf(stderr, "Warning: ");
	va_start(args, format);
	vfprintf(stderr, format, args);
	va_end(args);

	if (!loc)
		loc = &curr_loc;
	fprintf(stderr, ". at %s:%d\n", loc->f, loc->l);

	fflush(stderr);
}

#endif
