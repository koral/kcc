/* KPU back-end */

#include <ctype.h>
#include <inttypes.h>
#include "kpu_gen.h"
#include "ir.h"
#include "mem.h"
#include "kpu_reg.h"

char *reg_name[] = { "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8", "r9",
		     "r10", "r11", "r12", "r13", "r14", "r15", "r16", "r17",
		     "r18", "r19", "r20", "r21", "r22", "r23", "r24", "r25",
		     "fp", "sp", "r28", "r29", "st", "pc" };

struct curr_func_info_s {
	char *name;
	char *label;
	char *exit_label;
	unsigned tmp_regs;		/* number of temporaries reg used
					   (excluding parameters)*/
	unsigned params;		/* incoming parameters */
	unsigned frame_size;
	struct kpu_frag_s *prolog_pos;	/* where we'll backpatch the
					   prologue*/
};

static struct  curr_func_info_s curr_f;

static char *__kpu_op_t_str[] = {
	FOREACH_kpu_op_t(GENERATE_STRING)
};

static inline char *kpu_op_to_str(kpu_op_t op)
{
	return __kpu_op_t_str[op] + 4;
}

struct kpu_code_s kpu_code;

/* kind of op codes */
typedef enum  {
	RRR_OP,
	RRI_OP,
	RR_OP,
	RI_OP,
	R_OP,
	I_OP,
	VOID_OP
} op_kind;

static op_kind kpu_op_kind(kpu_op_t op)
{
	switch (op) {
	case KPU_add:
	case KPU_sub:
	case KPU_shr:
	case KPU_shl:
	case KPU_not:
	case KPU_and:
	case KPU_or:
	case KPU_xor:
	case KPU_mult:
	case KPU_div:
	case KPU_mod:
	case KPU_cmp:
		return RRR_OP;
	case KPU_addi:
	case KPU_subi:
	case KPU_shri:
	case KPU_shli:
	case KPU_andi:
	case KPU_ori:
	case KPU_xori:
	case KPU_multi:
	case KPU_divi:
	case KPU_modi:
	case KPU_ldw:
	case KPU_stw:
	case KPU_ldh:
	case KPU_sth:
	case KPU_ldhu:
	case KPU_sthu:
	case KPU_ldb:
	case KPU_stb:
	case KPU_ldbu:
	case KPU_stbu:
		return RRI_OP;
	case KPU_mov:
		return RR_OP;
	case KPU_cmpi:
	case KPU_movi:
	case KPU_ldaw:
	case KPU_staw:
		return RI_OP;
		break;
	case KPU_jmpr:
		return R_OP;
	case KPU_call:
	case KPU_jmp:
	case KPU_bo:
	case KPU_bg:
	case KPU_beq:
	case KPU_beg:
	case KPU_bgz:
	case KPU_bneq:
		return I_OP;
	case KPU_nop:
		return VOID_OP;
	default:
		ASSERT(false);
		break;
	}
}

static inline struct kpu_frag_s *kpu_frag_alloc(void)
{
	struct kpu_frag_s *frag;

	frag = xcalloc(sizeof(*frag));

	return frag;
}

static void kpu_check_frag(struct kpu_frag_s *frag)
{
	switch (frag->type) {
	case KPU_FRAG_INST:
		switch (kpu_op_kind(frag->op)) {
		case RRR_OP:
			if (!is_reg_valid(frag->arg[2].reg)) {
				err_warning(NULL, "Possible use of an "
					    "uninitialized variable");
				frag->arg[2].reg = 0;
			}
		case RRI_OP:
		case RR_OP:
			if (!is_reg_valid(frag->arg[1].reg)) {
				err_warning(NULL, "Possible use of an "
					    "uninitialized variable");
				frag->arg[1].reg = 0;
			}
		case RI_OP:
			if (!is_reg_valid(frag->arg[0].reg))
				ASSERT(false);
			break;
		default:
		break;
		}
		break;
	case KPU_FRAG_LABEL:
		ASSERT(frag->label);
		break;
	case KPU_FRAG_STR:
		ASSERT(frag->str);
		break;
	}

	return;
}

static void kpu_emit_frag(struct kpu_frag_s *frag)
{
	kpu_check_frag(frag);

	if (!kpu_code.first)
		kpu_code.first = frag;
	else
		kpu_code.last->next = frag;

	frag->prev = kpu_code.last;
	kpu_code.last = frag;
}

static void kpu_emit_frag_label(char *l)
{
	char *c = xmalloc(MAX_TMP_STR);
	struct kpu_frag_s *frag = kpu_frag_alloc();

	frag->type = KPU_FRAG_LABEL;
	frag->label = strncpy(c, l, MAX_TMP_STR);

	kpu_emit_frag(frag);
}

static void kpu_emit_frag_str(char *c)
{
	struct kpu_frag_s *frag = kpu_frag_alloc();

	frag->type = KPU_FRAG_STR;
	frag->label = c;

	kpu_emit_frag(frag);
}

/* generate a load for static allocate syms */
static char kpu_gen_load_sym(struct sym_s *sym)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();

	if (sym->c_type.ref_lev == 0) {
		switch (sym->c_type.t) {
		case BOOL_T:
		case U_CHAR_T:
			k_frag->op = KPU_ldbu;
			ASSERT(false);
			break;
		case CHAR_T:
			k_frag->op = KPU_ldb;
			k_frag->arg[0].reg = kpu_first_free_tmp_reg();
			k_frag->arg[1].reg = ZEROED_REG;
			k_frag->arg[2].label = xstrdup(sym->name);
			kpu_add_sym2reg(sym, k_frag->arg[0].reg);
			break;
		case SHORT_T:
			k_frag->op = KPU_ldh;
			k_frag->arg[0].reg = kpu_first_free_tmp_reg();
			k_frag->arg[1].reg = ZEROED_REG;
			k_frag->arg[2].label = xstrdup(sym->name);
			kpu_add_sym2reg(sym, k_frag->arg[0].reg);
			break;
		case U_SHORT_T:
			k_frag->op = KPU_ldhu;
			k_frag->arg[0].reg = kpu_first_free_tmp_reg();
			k_frag->arg[1].reg = ZEROED_REG;
			k_frag->arg[2].label = xstrdup(sym->name);
			kpu_add_sym2reg(sym, k_frag->arg[0].reg);
			break;
		case INT_T:
		case U_INT_T:
		case LONG_T:
		case U_LONG_T:
		case LONG_LONG_T:
		case U_LONG_LONG_T:
			k_frag->op = KPU_ldaw;
			k_frag->arg[0].reg = kpu_first_free_tmp_reg();
			k_frag->arg[1].label = xstrdup(sym->name);
			kpu_add_sym2reg(sym, k_frag->arg[0].reg);
			break;
		case VOID_T:
		case STRUCT_T:
		case UNION_T:
			ASSERT(false);
			break;
		}
	} else {
		/* pointer */
		k_frag->op = KPU_ldaw;
		k_frag->arg[0].reg = kpu_first_free_tmp_reg();
		k_frag->arg[1].label = xstrdup(sym->name);
		kpu_add_sym2reg(sym, k_frag->arg[0].reg);

	}
	kpu_emit_frag(k_frag);

	return k_frag->arg[0].reg;
}

/* generate a store for static allocate syms */
static void kpu_gen_store_sym(struct sym_s *sym)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();

	ASSERT(sym->c_type.ref_lev == 0);
	ASSERT(is_reg_valid(sym->__reg));

	switch (sym->c_type.t) {
	case BOOL_T:
	case U_CHAR_T:
		k_frag->op = KPU_stbu;
		ASSERT(false);
		break;
	case CHAR_T:
		k_frag->op = KPU_stb;
		k_frag->arg[0].reg = sym->__reg;
		k_frag->arg[1].reg = ZEROED_REG;
		k_frag->arg[2].label = xstrdup(sym->name);
		break;
	case SHORT_T:
		k_frag->op = KPU_sth;
		k_frag->arg[0].reg = sym->__reg;
		k_frag->arg[1].reg = ZEROED_REG;
		k_frag->arg[2].label = xstrdup(sym->name);
		break;
	case U_SHORT_T:
		k_frag->op = KPU_sthu;
		k_frag->arg[0].reg = sym->__reg;
		k_frag->arg[1].reg = ZEROED_REG;
		k_frag->arg[2].label = xstrdup(sym->name);
		break;
	case INT_T:
	case U_INT_T:
	case LONG_T:
	case U_LONG_T:
	case LONG_LONG_T:
	case U_LONG_LONG_T:
		k_frag->op = KPU_staw;
		k_frag->arg[0].reg = sym->__reg;
		k_frag->arg[1].label = xstrdup(sym->name);
		break;
	case VOID_T:
	case STRUCT_T:
	case UNION_T:
		ASSERT(false);
		break;
	}

	kpu_emit_frag(k_frag);
}

/* get the register of a sym, generate load in case */
static char kpu_get_sym_reg(struct sym_s *sym)
{
	if (is_reg_valid(sym->__reg))
		return sym->__reg;
	else
		/* register fill */
		return kpu_gen_load_sym(sym);
}

static void kpu_emit_mov(char dest, char src)
{
	struct kpu_frag_s *k_frag = kpu_frag_alloc();

	k_frag->op = KPU_mov;
	k_frag->arg[0].reg = dest;
	k_frag->arg[1].reg = src;
	kpu_emit_frag(k_frag);
}

static void kpu_emit_alu_inst(struct ir_statment_s *ir_frag,
			      kpu_op_t rrr_op,
			      kpu_op_t rri_op) {
	struct kpu_frag_s *k_frag = kpu_frag_alloc();

	k_frag->type = KPU_FRAG_INST;
	if (ir_frag->arg[0].type == IR_CONST &&
	    ir_frag->arg[1].type == IR_CONST) {
		/* constant propagation was missed */
		ASSERT(false);
	} else if (ir_frag->arg[0].type == IR_SYM &&
		   ir_frag->arg[1].type == IR_SYM) {
		k_frag->op = rrr_op;
		k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->arg[0].sym);
		k_frag->arg[2].reg = kpu_get_sym_reg(ir_frag->arg[1].sym);
		k_frag->arg[0].reg = kpu_get_reg(ir_frag);
	} else {
		struct sym_s *arg_sym;
		struct const_s *arg_const;

		if (ir_frag->arg[0].type == IR_SYM) {
			arg_sym = ir_frag->arg[0].sym;
			arg_const = &ir_frag->arg[1].imm;
		} else {
			arg_const = &ir_frag->arg[0].imm;
			arg_sym = ir_frag->arg[1].sym;
		}

		k_frag->op = rri_op;
		k_frag->arg[0].reg = kpu_get_reg(ir_frag);
		k_frag->arg[1].reg = kpu_get_sym_reg(arg_sym);
		k_frag->arg[2].imm.int64 = arg_const->int64;

	}
	kpu_emit_frag(k_frag);
}

static void kpu_emit_incr_sp(int off)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();
	if (off >= 0)
		k_frag->op = KPU_addi;
	else
		k_frag->op = KPU_subi;
	k_frag->arg[0].reg = STACK_POINTER;
	k_frag->arg[1].reg = STACK_POINTER;
	k_frag->arg[2].imm.int64 = abs(off);
	kpu_emit_frag(k_frag);
}

static void kpu_emit_save_reg(char n, char b_reg, int sp_off)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_stw;
	k_frag->arg[0].reg = n;
	k_frag->arg[1].reg = b_reg;
	k_frag->arg[2].imm.int64 = (int64_t)sp_off;
	kpu_emit_frag(k_frag);
}

static void kpu_emit_load_reg(char n, char b_reg, int sp_off)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_ldw;
	k_frag->arg[0].reg = n;
	k_frag->arg[1].reg = b_reg;
	k_frag->arg[2].imm.int64 = (int64_t)sp_off;
	kpu_emit_frag(k_frag);
}

static unsigned kpu_emit_push_reg(char n)
{
	kpu_emit_save_reg(n, STACK_POINTER, 0);
	kpu_emit_incr_sp(-4);

	return curr_f.frame_size += 4;
}

static unsigned kpu_emit_pop_reg(char n)
{
	kpu_emit_incr_sp(4);
	kpu_emit_load_reg(n, STACK_POINTER, 0);


	return curr_f.frame_size -= 4;
}

static void kpu_emit_func_prologue(void)
{
	int i, off, reg;

	kpu_emit_frag_label(curr_f.label);
	kpu_emit_frag_str("\t; prologue");

	kpu_emit_save_reg(FRAME_POINTER, STACK_POINTER, 0);
	kpu_emit_mov(FRAME_POINTER, STACK_POINTER);

	off = 4 * (curr_f.tmp_regs + 2);
	curr_f.frame_size = off;
	kpu_emit_incr_sp(-off);

	kpu_emit_save_reg(RET_ADDR_REG, FRAME_POINTER, -4);

	reg = REG_TMP_LAST;
	for (i = 0; i < curr_f.tmp_regs; i++)
		kpu_emit_save_reg(reg - i, FRAME_POINTER, -4 * (i + 2));

	kpu_emit_frag_str("\t; end prologue");
}

static void kpu_back_patch_prologue(void)
{
	struct kpu_code_s tmp = kpu_code;

	kpu_code.first = NULL;
	kpu_code.last = NULL;

	curr_f.tmp_regs = kpu_used_tmp_reg();

	kpu_emit_func_prologue();

 	kpu_code.last->next = curr_f.prolog_pos->next;
	curr_f.prolog_pos->next->prev = kpu_code.last;
	curr_f.prolog_pos->next = kpu_code.first;

	kpu_code = tmp;
}

static void kpu_emit_func_epilogue(void)
{
	struct kpu_frag_s *k_frag;
	int i, reg;

	kpu_emit_frag_label(curr_f.exit_label);

	kpu_emit_frag_str("\t; epilogue");

	kpu_emit_load_reg(RET_ADDR_REG, FRAME_POINTER, -4);
	reg = REG_TMP_LAST;
	for (i = 0; i < curr_f.tmp_regs; i++)
		kpu_emit_load_reg(reg - i, FRAME_POINTER, -4 * (i + 2));

	kpu_emit_mov(STACK_POINTER, FRAME_POINTER);

	kpu_emit_load_reg(FRAME_POINTER, STACK_POINTER, 0);

	k_frag = kpu_frag_alloc();
	k_frag->type = KPU_FRAG_INST;
	k_frag->op = KPU_jmpr;
	k_frag->arg[0].reg = RET_ADDR_REG;
	kpu_emit_frag(k_frag);

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_nop;
	kpu_emit_frag(k_frag);
}

static void kpu_emit_ret_inst(struct ir_statment_s *ir_frag)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();
	k_frag->type = KPU_FRAG_INST;
	k_frag->arg[0].reg = RET_REG;
	switch (ir_frag->res.type) {
	case IR_VOID:
		break;
	case IR_SYM:
		k_frag->op = KPU_mov;
		k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->res.sym);
		kpu_emit_frag(k_frag);
		break;
	case IR_CONST:
		k_frag->op = KPU_movi;
		k_frag->arg[1].imm.int64 =
			ir_frag->res.imm.int64;
		kpu_emit_frag(k_frag);
		break;
	default:
		ASSERT(false);
		break;
	}

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_jmp;
	k_frag->arg[0].label = xstrdup(curr_f.exit_label);
	kpu_emit_frag(k_frag);

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_nop;
	kpu_emit_frag(k_frag);

}

static void kpu_emit_cmp(struct ir_statment_s *ir_frag, kpu_op_t op)
{
	struct kpu_frag_s *k_frag;

	ASSERT(ir_frag->arg[0].type == IR_SYM);

	k_frag = kpu_frag_alloc();
	switch (ir_frag->arg[1].type) {
	case IR_CONST:
		k_frag->op = KPU_cmpi;
		k_frag->arg[0].reg = kpu_get_sym_reg(ir_frag->arg[0].sym);
		k_frag->arg[1].imm.int64 = ir_frag->arg[1].imm.int64;
		break;
	case IR_SYM:
		k_frag->op = KPU_cmp;
		k_frag->arg[0].reg = kpu_get_sym_reg(ir_frag->arg[0].sym);
		k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->arg[1].sym);
		break;
	default:
		/* constant propagation missed? */
		ASSERT(false);
		break;
	}
	kpu_emit_frag(k_frag);

	k_frag = kpu_frag_alloc();
	k_frag->op = op;
	k_frag->arg[0].label = xstrdup(ir_frag->res.label);
	kpu_emit_frag(k_frag);

	k_frag = kpu_frag_alloc();
	k_frag->op = KPU_nop;
	kpu_emit_frag(k_frag);
}

static void kpu_emit_load_store_offset(struct ir_statment_s *ir_frag, bool is_store)
{
	struct kpu_frag_s *k_frag;

	k_frag = kpu_frag_alloc();
	switch (ir_frag->arg[0].sym->c_type.t) {
	case BOOL_T:
	case U_CHAR_T:
		k_frag->op = is_store ? KPU_stbu : KPU_ldbu;
		break;
	case CHAR_T:
		k_frag->op = is_store ? KPU_stb : KPU_ldb;
		break;
	case SHORT_T:
		k_frag->op = is_store ? KPU_sth : KPU_ldh;
		break;
	case U_SHORT_T:
		k_frag->op = is_store ? KPU_sthu : KPU_ldhu;
		break;
	case INT_T:
	case U_INT_T:
	case LONG_T:
	case U_LONG_T:
	case LONG_LONG_T:
	case U_LONG_LONG_T:
		k_frag->op = is_store ? KPU_stw : KPU_ldw;
		break;
	case VOID_T:
	case STRUCT_T:
	case UNION_T:
		ASSERT(false);
		break;
	}

	k_frag->arg[0].reg = kpu_get_reg(ir_frag);
	k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->arg[0].sym);
	k_frag->arg[2].imm.int64 = ir_frag->arg[1].imm.int64;
	kpu_emit_frag(k_frag);
}

static void kpu_push_arg(struct ir_statment_s *ir_frag)
{
	struct kpu_frag_s *k_frag;

	switch (ir_frag->res.type) {
	case IR_SYM:
		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_mov;
		k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->res.sym);
		break;
	case IR_CONST:
		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_movi;
		k_frag->arg[1].imm.int64 =
			ir_frag->res.imm.int64;
		break;
	default:
		ASSERT(false);
		break;
	}

	k_frag->arg[0].reg = kpu_get_param_reg(kpu_emit_push_reg);
	kpu_emit_frag(k_frag);
}

static void kpu_emit_frag_inst(struct ir_statment_s *ir_frag)
{
	struct kpu_frag_s *k_frag;

	switch ((int)ir_frag->op) {
	case IR_FUNC_OP:
		kpu_reset_param_reg();
		kpu_clean_tmp_regs();

		curr_f.name = ir_frag->res.sym->name;
		curr_f.prolog_pos = kpu_code.last;

		if (!curr_f.label) {
			curr_f.label = xmalloc(MAX_TMP_STR);
			curr_f.exit_label = xmalloc(MAX_TMP_STR);
		}
		snprintf(curr_f.label, MAX_TMP_STR, "%s", curr_f.name);
		snprintf(curr_f.exit_label, MAX_TMP_STR, "$L%s_exit", curr_f.name);
		curr_f.params = ir_frag->arg[1].imm.u_int64;
		break;
	case IR_PARAM_OP:
		kpu_tmp_add(ir_frag->res.sym);
		break;
	case IR_PASS_OP:
		kpu_push_arg(ir_frag);
		break;
	case IR_CALL_OP:
		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_call;
		k_frag->arg[0].label = xstrdup(ir_frag->arg[0].sym->name);
		kpu_emit_frag(k_frag);

		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_nop;
		kpu_emit_frag(k_frag);

		kpu_fill_regs(kpu_emit_pop_reg);

		if (ir_frag->res.sym)
			/* mov Rx R0 */
			kpu_emit_mov(kpu_get_reg(ir_frag), RET_REG);
		break;
	case IR_RET_OP:
		kpu_emit_ret_inst(ir_frag);
		break;
	case '=':
		k_frag = kpu_frag_alloc();
		if (ir_frag->arg[0].type == IR_SYM) {
			k_frag->op = KPU_mov;
			k_frag->arg[0].reg = kpu_get_reg(ir_frag);
			k_frag->arg[1].reg = kpu_get_sym_reg(ir_frag->arg[0].sym);
		} else {
			k_frag->op = KPU_movi;
			k_frag->arg[0].reg = kpu_get_reg(ir_frag);

			k_frag->arg[1].imm.int64 =
				ir_frag->arg[0].imm.int64;
		}
		kpu_emit_frag(k_frag);
		break;
	case '+':
		kpu_emit_alu_inst(ir_frag, KPU_add, KPU_addi);
		break;
	case '-':
		kpu_emit_alu_inst(ir_frag, KPU_sub, KPU_subi);
		break;
	case '*':
		kpu_emit_alu_inst(ir_frag, KPU_mult, KPU_multi);
		break;
	case '/':
		kpu_emit_alu_inst(ir_frag, KPU_div, KPU_divi);
		break;
	case '%':
		kpu_emit_alu_inst(ir_frag, KPU_mod, KPU_modi);
		break;
	case '&':
		kpu_emit_alu_inst(ir_frag, KPU_and, KPU_andi);
		break;
	case '|':
		kpu_emit_alu_inst(ir_frag, KPU_or, KPU_ori);
		break;
	case '^':
		kpu_emit_alu_inst(ir_frag, KPU_not, KPU_nop);
		break;
	case IR_IFEQ_OP:
		kpu_emit_cmp(ir_frag, KPU_beq);
		break;
	case IR_IFNEQ_OP:
		kpu_emit_cmp(ir_frag, KPU_bneq);
		break;
	case IR_IFGE_OP:
		kpu_emit_cmp(ir_frag, KPU_bg);
		break;
	case IR_IFLE_OP:
		/* comparison not in canonical form */
		ASSERT(false);
		break;
	case IR_IFGEQ_OP:
		kpu_emit_cmp(ir_frag, KPU_beg);
		break;
	case IR_IFLEQ_OP:
		/* comparison not in canonical form */
		ASSERT(false);
		break;
	case IR_GOTO_OP:
		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_jmp;
		k_frag->arg[0].label = xstrdup(ir_frag->res.label);
		kpu_emit_frag(k_frag);

		k_frag = kpu_frag_alloc();
		k_frag->op = KPU_nop;
		kpu_emit_frag(k_frag);
		break;
	case IR_LOAD_OP:
		kpu_emit_load_store_offset(ir_frag, false);
		break;
	case IR_LDST_OP:
		kpu_gen_load_sym(ir_frag->arg[0].sym);
		break;
	case IR_STORE_OP:
		kpu_emit_load_store_offset(ir_frag, true);
		break;
	case IR_STRST_OP:
		kpu_gen_store_sym(ir_frag->arg[0].sym);
		break;
	case IR_REF_OP:
		k_frag = kpu_frag_alloc();
		k_frag->arg[0].reg = kpu_get_reg(ir_frag);
		k_frag->op = KPU_movi;
		k_frag->arg[1].label = xstrdup(ir_frag->arg[0].sym->name);
		kpu_emit_frag(k_frag);
		break;
	case IR_ENDFUNC_OP:
		kpu_back_patch_prologue();
		kpu_emit_func_epilogue();
		memset(&curr_f, 0 , sizeof(curr_f));
		break;
	default:
		ASSERT(false);
		return;
	}
}

static void kpu_emit(struct ir_statment_s *frag)
{
	curr_loc.l = frag->src_line;

	switch (frag->type) {
	case IR_FRAG_INST:
		kpu_emit_frag_inst(frag);
		break;
	case IR_FRAG_LABEL:
		kpu_emit_frag_label(frag->label);
		break;
	case IR_FRAG_STR:
		kpu_emit_frag_str(frag->str);
		break;
	}
}

static void kpu_dump_imm(union kpu_operand_s *op)
{
	if (op->label)
		printf("%s", op->label);
	else
		printf("%" PRId64,
		       op->imm.int64);
}

static void kpu_frag_inst_dump(struct kpu_frag_s *frag)
{
	switch (kpu_op_kind(frag->op)) {
	case RRR_OP:
		printf("\t%s %s, %s, %s\n", kpu_op_to_str(frag->op),
		       reg_name[(int)frag->arg[0].reg],
		       reg_name[(int)frag->arg[1].reg],
		       reg_name[(int)frag->arg[2].reg]);
		break;
	case RRI_OP:
		printf("\t%s %s, %s, ", kpu_op_to_str(frag->op),
		       reg_name[(int)frag->arg[0].reg],
		       reg_name[(int)frag->arg[1].reg]);
		kpu_dump_imm(&frag->arg[2]);
		putchar('\n');
		break;
	case RR_OP:
		printf("\t%s %s, %s\n", kpu_op_to_str(frag->op),
		       reg_name[(int)frag->arg[0].reg],
		       reg_name[(int)frag->arg[1].reg]);
		break;
	case RI_OP:
		printf("\t%s %s, ", kpu_op_to_str(frag->op),
		       reg_name[(int)frag->arg[0].reg]);
		kpu_dump_imm(&frag->arg[1]);
		putchar('\n');
		break;
	case R_OP:
		printf("\t%s %s\n", kpu_op_to_str(frag->op),
		       reg_name[(int)frag->arg[0].reg]);
		break;
	case I_OP:
		printf("\t%s ", kpu_op_to_str(frag->op));
		kpu_dump_imm(&frag->arg[0]);
		putchar('\n');
		break;
	case VOID_OP:
		printf("\t%s\n", kpu_op_to_str(frag->op));
		break;
	default:
		ASSERT(false);
		break;
	}
}

static void kpu_frag_dump(struct kpu_frag_s *frag)
{
	switch (frag->type) {
	case KPU_FRAG_INST:
		kpu_frag_inst_dump(frag);
		break;
	case KPU_FRAG_LABEL:
		printf("%s:\n", frag->label);
		break;
	case KPU_FRAG_STR:
		printf("%s\n", frag->str);
		break;
	}
}

void kpu_asm_gen(void)
{
	struct ir_statment_s *frag;

	frag = ir_code.first;

	while (frag) {
		kpu_emit(frag);
		frag = frag->next;
	}
}

void kpu_asm_dump(void)
{
	struct kpu_frag_s *k_frag = kpu_code.first;

	printf(";;; KPU assembly code\n"
               ";;; Generated by kcc https://github.com/AndreaCorallo/kcc\n\n");

	while (k_frag) {
		kpu_frag_dump(k_frag);
		k_frag = k_frag->next;
	}
}

void kpu_asm_free(void)
{
	struct kpu_frag_s *next;
	struct kpu_frag_s *k_frag = kpu_code.first;

	while (k_frag) {
		next = k_frag->next;
		kpu_free_frag(k_frag);
		k_frag = next;
	}

	if (curr_f.label)
		XFREE(curr_f.label);

	if (curr_f.exit_label)
		XFREE(curr_f.exit_label);
}
