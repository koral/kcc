# kcc
A C compiler targeting kpu.

**This project is work in progress!**

### Compilation:
Output verbosity is controlled at compile time as follow:

```make CFLAGS=-DKCC_DEBUG=0```			just assembly target code is generated

```make CFLAGS=-DKCC_DEBUG=DBG_PARSER```	dump parse debug info

```make CFLAGS=-DKCC_DEBUG=DBG_GEN_DAG```	dump DAG generation function calls

```make CFLAGS=-DKCC_DEBUG=DBG_DRAW_DAG```	dump the whole DAG forest

```make CFLAGS=-DKCC_DEBUG=DBG_DUMP_HASH```	dump the DAG hash content

```make CFLAGS=-DKCC_DEBUG=DBG_DUMP_IR```	dump the 3 addr intermediate representation content

```make```					all the previous debug info + the assembly target code are dumped

Two or more options can be combined as follow:

```make CFLAGS=-DKCC_DEBUG=DBG_DRAW_DAG|DBG_DUMP_IR```

## ABI
A basic description of the calling and stack convention used by kcc can be found [here](https://github.com/AndreaCorallo/kpu/wiki/Application-Binary-Interface#kcc-abi).

## Usage
kcc simply eats C code from standard input and produce output into standard output.

Use as follow:

```kcc$ ./kcc < file.c > out.s ```

## Testsuite
If a copy of the KPU repo (https://github.com/AndreaCorallo/kpu) is cloned a the same level of the kcc one is possible to run the test-suite on top of the RTL simulation as follow:
```
kcc$ cd tests/
tests$ make
```

## Output example
Sample C code ```tests/0017_tb.c```:
```
int a = 4;
int b = 1;

int main(void)
{
	if (a > 10 || !b)
		a = 8;
	else
		a = 1;

	return a;
}
```

### Parsing debug info + DAG generation function calls:
```
kcc$ ./kcc < tests/0017_tb.c
kcc parser: reduce at parser.y:343
kcc mk_leaf_type_spec() type specifier 'INT_T' at dag.c:337
kcc parser: reduce at parser.y:311
kcc parser: reduce at parser.y:447
kcc mk_leaf_id() id 'a' at dag.c:319
kcc parser: reduce at parser.y:443
kcc parser: reduce at parser.y:102
kcc mk_leaf_const_str() at dag.c:257
kcc parser: reduce at parser.y:95
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:542
kcc parser: reduce at parser.y:326
kcc mk_node() op '=' at dag.c:397
kcc parser: reduce at parser.y:321
kcc parser: reduce at parser.y:288
kcc mk_node() op 'DECLARATION_OP' at dag.c:397
kcc parser: reduce at parser.y:691
kcc parser: reduce at parser.y:89
kcc parser: reduce at parser.y:343
kcc mk_leaf_type_spec() type specifier 'INT_T' at dag.c:337
kcc parser: reduce at parser.y:311
kcc parser: reduce at parser.y:447
kcc mk_leaf_id() id 'b' at dag.c:319
kcc parser: reduce at parser.y:443
kcc parser: reduce at parser.y:102
kcc mk_leaf_const_str() at dag.c:257
kcc parser: reduce at parser.y:95
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:542
kcc parser: reduce at parser.y:326
kcc mk_node() op '=' at dag.c:397
kcc parser: reduce at parser.y:321
kcc parser: reduce at parser.y:288
kcc mk_node() op 'DECLARATION_OP' at dag.c:397
kcc parser: reduce at parser.y:691
kcc parser: reduce at parser.y:90
kcc parser: reduce at parser.y:343
kcc mk_leaf_type_spec() type specifier 'INT_T' at dag.c:337
kcc parser: reduce at parser.y:311
kcc parser: reduce at parser.y:447
kcc mk_leaf_id() id 'main' at dag.c:319
kcc parser: reduce at parser.y:340
kcc mk_leaf_type_spec() type specifier 'VOID_T' at dag.c:337
kcc parser: reduce at parser.y:311
kcc parser: reduce at parser.y:496
kcc parser: reduce at parser.y:489
kcc parser: reduce at parser.y:485
kcc parser: reduce at parser.y:458
kcc mk_node() op 'DIRECT_DECL_OP' at dag.c:397
kcc parser: reduce at parser.y:443
kcc parser: reduce at parser.y:94
kcc mk_leaf_id() id 'a' at dag.c:319
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:102
kcc mk_leaf_const_str() at dag.c:257
kcc parser: reduce at parser.y:95
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:202
kcc mk_node() op '>' at dag.c:397
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:172
kcc parser: reduce at parser.y:94
kcc mk_leaf_id() id 'b' at dag.c:319
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:157
kcc mk_node() op '!' at dag.c:397
kcc mk_leaf_const() at dag.c:232
kcc mk_node() op 'EQ_OP' at dag.c:397
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:245
kcc mk_node() op 'OR_OP' at dag.c:397
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:277
kcc parser: reduce at parser.y:94
kcc mk_leaf_id() id 'a' at dag.c:319
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:263
kcc parser: reduce at parser.y:102
kcc mk_leaf_const_str() at dag.c:257
kcc parser: reduce at parser.y:95
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:259
kcc mk_node() op '=' at dag.c:397
kcc parser: reduce at parser.y:277
kcc parser: reduce at parser.y:610
kcc parser: reduce at parser.y:573
kcc parser: reduce at parser.y:94
kcc mk_leaf_id() id 'a' at dag.c:319
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:263
kcc parser: reduce at parser.y:102
kcc mk_leaf_const_str() at dag.c:257
kcc parser: reduce at parser.y:95
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:259
kcc mk_node() op '=' at dag.c:397
kcc parser: reduce at parser.y:277
kcc parser: reduce at parser.y:610
kcc parser: reduce at parser.y:573
kcc parser: reduce at parser.y:617
kcc mk_node() op 'IF_ELSE_OP' at dag.c:397
kcc parser: reduce at parser.y:574
kcc parser: reduce at parser.y:605
kcc parser: reduce at parser.y:599
kcc parser: reduce at parser.y:94
kcc mk_leaf_id() id 'a' at dag.c:319
kcc parser: reduce at parser.y:131
kcc parser: reduce at parser.y:153
kcc parser: reduce at parser.y:176
kcc parser: reduce at parser.y:181
kcc parser: reduce at parser.y:188
kcc parser: reduce at parser.y:194
kcc parser: reduce at parser.y:200
kcc parser: reduce at parser.y:208
kcc parser: reduce at parser.y:214
kcc parser: reduce at parser.y:219
kcc parser: reduce at parser.y:224
kcc parser: reduce at parser.y:229
kcc parser: reduce at parser.y:241
kcc parser: reduce at parser.y:253
kcc parser: reduce at parser.y:258
kcc parser: reduce at parser.y:277
kcc parser: reduce at parser.y:685
kcc mk_node() op 'RETURN_OP' at dag.c:397
kcc parser: reduce at parser.y:576
kcc parser: reduce at parser.y:605
kcc parser: reduce at parser.y:600
kcc mk_node() op ';' at dag.c:397
kcc parser: reduce at parser.y:595
kcc mk_node() op 'CODE_BLOCK_OP' at dag.c:397
kcc parser: reduce at parser.y:701
kcc mk_node() op 'FUNC_DECL_OP' at dag.c:397
kcc parser: reduce at parser.y:689
kcc parser: reduce at parser.y:90
kcc sym_add_curr_scope() add sym 'a' of type INT to 0x1350060 at dag.c:478
kcc sym_add_curr_scope() add sym 'b' of type INT to 0x1350060 at dag.c:478
kcc sym_add_curr_scope() add sym 'main' of type INT to 0x1350060 at dag.c:478
```

### DAG hash table content:
```
Dag nodes hash table 0x1350010 content:
0: 0x1355770 0x13564c0 0x13565e0
1: 0x13560d0 0x13556f0
2: 0x1356ae0 0x1356930 0x13555f0
3: 0x1356c60 0x1356890 0x1356000 0x1356320
4: 0x1355690
5: 0x1356ce0
6: 0x1356130 0x1356230 0x1356660
7:
8: 0x1356810 0x13563a0
9: 0x1356be0 0x1356b60
10: 0x1356440
11: 0x13561b0 0x1356560
12:
13: 0x1355550
14: 0x1356a30 0x13566e0
15: 0x13569d0
```

### DAG forest dump:
```
Graph 0:

                      [EXT_DECLARATION_OP](VOID)(0x1355770)
                                        |
                |---------------------------------------|
                |                                       |
 type_spec(INT_T)(INT)(0x1355550)              [=](INT)(0x13556f0)
                                                        |
                                            |----------------------|
                                            |                      |
                                  id(a)(INT)(0x13555f0) const(4)(INT)(0x1355690)


Graph 1:

                      [EXT_DECLARATION_OP](VOID)(0x13561b0)
                                        |
                |---------------------------------------|
                |                                       |
 type_spec(INT_T)(INT)(0x1355550)              [=](INT)(0x1356130)
                                                        |
                                            |----------------------|
                                            |                      |
                                  id(b)(INT)(0x1356000) const(1)(INT)(0x13560d0)


Graph 2:

                                                                                                                                            [FUNC_DECL_OP](VOID)(0x1356ce0)
                                                                                                                                                           |
                |----------------------------|-----------------------------|------------------------------------------------------------------------------------------------------------------------------|
                |                            |                             |                                                                                                                              |
 type_spec(INT_T)(INT)(0x1355550) id(main)(INT)(0x1356230) type_spec(VOID_T)(VOID)(0x1356320)                                                                                             [CODE_BLOCK_OP](VOID)(0x1356c60)
                                                                                                                                                                                                          |
                                                                                                                                                                                                          |
                                                                                                                                                                                                          |
                                                                                                                                                                                                [;](VOID)(0x1356be0)
                                                                                                                                                                                                          |
                                                                                                                                                                                           |------------------------------------------------------------------------------------------------------------|
                                                                                                                                                                                           |                                                                                                            |
                                                                                                                                                                             [IF_ELSE_OP](VOID)(0x1356a30)                                                                                 [RETURN_OP](VOID)(0x1356b60)
                                                                                                                                                                                           |                                                                                                            |
                                                                                                                                            |---------------------------------------------------------------------------------------------------------------------|                                  |---
                                                                                                                                            |                                                                      |                                              |                                  |
                                                                                                                                 [OR_OP](INT)(0x13566e0)                                                  [=](INT)(0x1356890)                            [=](INT)(0x13569d0)               id(a)(INT)(0x1356ae0)
                                                                                                                                            |                                                                      |                                              |
                                                                                                                     |----------------------------------------------|                                  |----------------------|                       |----------------------|
                                                                                                                     |                                              |                                  |                      |                       |                      |
                                                                                                            [>](INT)(0x13564c0)                          [EQ_OP](INT)(0x1356660)             id(a)(INT)(0x13555f0) const(8)(INT)(0x1356810) id(a)(INT)(0x1356930) const(1)(INT)(0x13560d0)
                                                                                                                     |                                              |
                                                                                                        |-----------------------|                       |----------------------|
                                                                                                        |                       |                       |                      |
                                                                                              id(a)(INT)(0x13555f0) const(10)(INT)(0x1356440) id(b)(INT)(0x1356000) const(0)(INT)(0x13565e0)

```
### 3 address intermediate rappresentation content:

```
IR content
----------------------------------------------------------------------------------------
inst n	|	label	|	opr	|	res	|	arg1	|	arg2	|
----------------------------------------------------------------------------------------
	.section .data
	|	b:	|		|		|		|		|
		.word 0x1
	|	a:	|		|		|		|		|
		.word 0x4
	.section .text
1	|		|	FUNC	|	main	|	0	|	0	|
2	|		|	IFGE	|	$Lt_0	|	a	|	10	|
3	|		|	IFEQ	|	$Lt_0	|	b	|	0	|
	|	$Lf_1:	|		|		|		|		|
4	|		|	=	|	a	|	1	|		|
5	|		|	GOTO	|	$Le_2	|		|		|
	|	$Lt_0:	|		|		|		|		|
6	|		|	=	|	a	|	8	|		|
	|	$Le_2:	|		|		|		|		|
7	|		|	RET	|	a	|		|		|
8	|		|	ENDFUNC	|		|		|		|
```

## Target assembly generated code:
```
;;; KPU assembly code
;;; Generated by kcc https://github.com/AndreaCorallo/kcc

.section .data
b:
	.word 0x1
a:
	.word 0x4
.section .text
main:
	; prologue
	stw fp, sp, 0
	mov fp, sp
	subi sp, sp, 16
	stw r28, fp, -4
	stw r24, fp, -8
	stw r23, fp, -12
	; end prologue
	ldaw r24, a
	cmpi r24, 10
	bg $Lt_0
	nop
	ldaw r23, b
	cmpi r23, 0
	beq $Lt_0
	nop
$Lf_1:
	jmp $Le_2
	movi r24, 1
$Lt_0:
	movi r24, 8
$Le_2:
	mov r0, r24
$Lmain_exit:
	; epilogue
	ldw r28, fp, -4
	ldw r24, fp, -8
	ldw r23, fp, -12
	mov sp, fp
	jmpr r28
	ldw fp, sp, 0
```

## Licensing:
GPL V3.

## Author:
Andrea Corallo <andrea_corallo@yahoo.it>

Copyright (c) 2017 by Andrea Corallo.
