#ifndef __KPU_GEN_H__
#define __KPU_GEN_H__

#include <stdlib.h>
#include "kcc.h"
#include "dag.h"

/* kpu op codes */
#define FOREACH_kpu_op_t(kpu_op_t)		\
		kpu_op_t(KPU_nop)		\
		kpu_op_t(KPU_ldw)		\
		kpu_op_t(KPU_stw)		\
		kpu_op_t(KPU_ldh)		\
		kpu_op_t(KPU_sth)		\
		kpu_op_t(KPU_ldhu)		\
		kpu_op_t(KPU_sthu)		\
		kpu_op_t(KPU_ldb)		\
		kpu_op_t(KPU_stb)		\
		kpu_op_t(KPU_ldbu)		\
		kpu_op_t(KPU_stbu)		\
		kpu_op_t(KPU_ldaw)		\
		kpu_op_t(KPU_staw)		\
		kpu_op_t(KPU_mov)		\
		kpu_op_t(KPU_movi)		\
		kpu_op_t(KPU_add)		\
		kpu_op_t(KPU_sub)		\
		kpu_op_t(KPU_shr)		\
		kpu_op_t(KPU_shl)		\
		kpu_op_t(KPU_not)		\
		kpu_op_t(KPU_and)		\
		kpu_op_t(KPU_or)		\
		kpu_op_t(KPU_xor)		\
		kpu_op_t(KPU_mult)		\
		kpu_op_t(KPU_div)		\
		kpu_op_t(KPU_mod)		\
		kpu_op_t(KPU_cmp)		\
		kpu_op_t(KPU_addi)		\
		kpu_op_t(KPU_subi)		\
		kpu_op_t(KPU_shri)		\
		kpu_op_t(KPU_shli)		\
		kpu_op_t(KPU_andi)		\
		kpu_op_t(KPU_ori)		\
		kpu_op_t(KPU_xori)		\
		kpu_op_t(KPU_multi)		\
		kpu_op_t(KPU_divi)		\
		kpu_op_t(KPU_modi)		\
		kpu_op_t(KPU_cmpi)		\
		kpu_op_t(KPU_jmp)		\
		kpu_op_t(KPU_bo)		\
		kpu_op_t(KPU_bg)		\
		kpu_op_t(KPU_beq)		\
		kpu_op_t(KPU_beg)		\
		kpu_op_t(KPU_bgz)		\
		kpu_op_t(KPU_bneq)		\
		kpu_op_t(KPU_jmpr)		\
		kpu_op_t(KPU_call)		\
		kpu_op_t(KPU_int)		\
		kpu_op_t(KPU_LAST)

typedef enum {
FOREACH_kpu_op_t(GENERATE_ENUM)
} kpu_op_t;

typedef enum {
	KPU_FRAG_INST,
	KPU_FRAG_LABEL,
	KPU_FRAG_STR
} kpu_frag_t;

/* one kpu operand */
union kpu_operand_s {
	char reg;
	struct {
		struct const_s imm;
		char *label;
	};
};

/* a kpu asm fragment */
struct kpu_frag_s {
	struct kpu_frag_s *prev;
	struct kpu_frag_s *next;
	kpu_op_t op;
	kpu_frag_t type;
	union {
		char *label;
		char *str;
		union kpu_operand_s arg[3];
	};
};

struct kpu_code_s {
	struct kpu_frag_s *first;
	struct kpu_frag_s *last;
};

extern struct kpu_code_s kpu_code;

static inline void kpu_free_frag(struct kpu_frag_s *k_frag)
{
	if (k_frag->type == KPU_FRAG_LABEL)
		free(k_frag->label);
	if (k_frag->arg[0].label)
		free(k_frag->arg[0].label);
	if (k_frag->arg[1].label)
		free(k_frag->arg[1].label);
	if (k_frag->arg[2].label)
		free(k_frag->arg[2].label);
	free(k_frag);
}

void kpu_asm_gen(void);

void kpu_asm_dump(void);

void kpu_asm_free(void);

#endif
