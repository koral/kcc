#include "tree_optim.h"

static void const_prop(struct node_s *node)
{
	if (TYPE(node) != OPR_T)
		return;

	if (node->op.nops != 2 ||
		TYPE(CHILD(node, 0)) != CONST_T ||
		TYPE(CHILD(node, 1)) != CONST_T)
		return;

	switch ((int)OP(node)) {
	case '+':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 +
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '-':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 -
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '*':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 *
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '/':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 /
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '%':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 %
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '<':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 <
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case '>':
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 >
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_LE_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 <=
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_GE_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 >=
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_NE_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 !=
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_AND_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 &&
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_OR_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 ||
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	case K_EQ_OP:
		node->constant.u_int64 =
			CHILD(node, 0)->constant.u_int64 ==
			CHILD(node, 1)->constant.u_int64;
		SET_TYPE(node, CONST_T);
		break;
	default:
		break;
	}
}

static void tree_const_prop(void)
{
	dag_walk(NULL, const_prop);
}

void tree_optimizer(void)
{
	tree_const_prop();
}
