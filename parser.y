%{
#include <stdio.h>
#include "kcc.h"
#include "dag.h"
#include "mem.h"

int yylex(void);
void yyerror(const char *str);

#if (KCC_DEBUG & DBG_PARSER)
#define TRACE printf("kcc parser: reduce at %s:%d\n", __FILE__, __LINE__)
#else
#define TRACE
#endif

#define UNSUPPORTED err_internal_fatal(NULL, \
	"unsupported syntax construct at %s:%d", __FILE__, __LINE__)

static struct node_s *expand_bool(struct node_s *node)
{
	op_t op;

	if (TYPE(node) == OPR_T) {
		if (is_op_t_cmp_bool(OP(node))) {
			return node;
		}
		else if (OP(node) == '!') {
			op = K_EQ_OP;
			node = CHILD(node, 0);
		}
		else {
			op = K_NE_OP;
		}
	}
	else {
		op = K_NE_OP;
	}

	return  mk_node(op, node->src_line, false, 2, node,
			mk_leaf_const(0, INT_T, 0));
}

static struct node_s *expand_prefix_op(char op, struct node_s *node)
{
	struct node_s *plus = mk_node(op, node->src_line, true, 2, node,
				mk_leaf_const(1, INT_T, node->src_line));

	return mk_node('=', node->src_line, true, 2, node, plus);
}

static struct node_s *expand_postfix_op(char op, struct node_s *node)
{

	return mk_node(K_POSTFIX_OP, node->src_line, true, 2, node,
		 expand_prefix_op(op, node));
}

%}

%output "parser.c"
%defines "parser.h"
%error-verbose

%union	{
		struct {
			char *str;
			unsigned line;
		} s;
		struct node_s *node;
		struct {
			unsigned oper;	/* Should be op_t */
			unsigned line;
		} op;
		struct {
			int t;
			unsigned line;
		} tk;
}

%token	<s> IDENTIFIER I_CONSTANT STRING_LITERAL

%token	<tk> '*' '/' '+' '-' '<' '>' '&' '^' '%' '|' '(' ')' ',' '{' '}' '~' '!' '[' ']'
%token	<tk> F_CONSTANT FUNC_NAME SIZEOF
%token	<tk> PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token	<tk> AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token	<tk> SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token	<tk> XOR_ASSIGN OR_ASSIGN
%token	<tk> TYPEDEF_NAME ENUMERATION_CONSTANT
%token	<tk> TYPEDEF EXTERN STATIC AUTO REGISTER INLINE
%token	<tk> CONST RESTRICT VOLATILE
%token	<tk> BOOL CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE VOID
%token	<tk> COMPLEX IMAGINARY
%token	<tk> STRUCT UNION ENUM ELLIPSIS
%token	<tk> CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN
%token	<tk> ALIGNAS ALIGNOF ATOMIC GENERIC NORETURN STATIC_ASSERT THREAD_LOCAL

%type	<node> primary_expression postfix_expression unary_expression
%type	<node> cast_expression multiplicative_expression shift_expression relational_expression equality_expression and_expression exclusive_or_expression inclusive_or_expression logical_and_expression logical_or_expression conditional_expression assignment_expression expression statement expression_statement additive_expression compound_statement selection_statement iteration_statement jump_statement block_item block_item_list external_declaration declaration function_definition type_specifier storage_class_specifier declaration_specifiers type_qualifier function_specifier alignment_specifier init_declarator_list init_declarator declarator direct_declarator pointer constant parameter_type_list parameter_list parameter_declaration argument_expression_list labeled_statement constant_expression initializer
%type	<op> assignment_operator
%type	<tk> unary_operator
%type   <node> string

%%

translation_unit
	: external_declaration			{ TRACE; dag_add($1); }
	| translation_unit external_declaration	{ TRACE; dag_add($2); }
	;

primary_expression
	: IDENTIFIER	{ TRACE; $$ = mk_leaf_id($1.str, $1.line); }
	| constant	{ TRACE; $$ = $1; }
	| string	{ TRACE; $$ = $1; }
	| '(' expression ')'	{ TRACE; $$ = $2; }
	| generic_selection	{ TRACE; UNSUPPORTED; }
	;

constant
	: I_CONSTANT	{ TRACE; $$ = mk_leaf_const_str($1.str, $1.line); }	/* includes character_constant */
	| F_CONSTANT	{ TRACE; UNSUPPORTED; }
	| ENUMERATION_CONSTANT	{ TRACE; UNSUPPORTED; }	/* after it has been defined as such */
	;

enumeration_constant		/* before it has been defined as such */
	: IDENTIFIER	{ TRACE; UNSUPPORTED; }
	;

string
	: STRING_LITERAL	{ TRACE; $$ = mk_leaf_str_litt_id($1.str, $1.line); }
	| FUNC_NAME	{ TRACE; UNSUPPORTED; }
	;

generic_selection
	: GENERIC '(' assignment_expression ',' generic_assoc_list ')'	{ TRACE;  UNSUPPORTED; }
	;

generic_assoc_list
	: generic_association	{ TRACE; UNSUPPORTED; }
	| generic_assoc_list ',' generic_association	{ TRACE; UNSUPPORTED; }
	;

generic_association
	: type_name ':' assignment_expression	{ TRACE; UNSUPPORTED; }
	| DEFAULT ':' assignment_expression	{ TRACE; UNSUPPORTED; }
	;

postfix_expression
	: primary_expression	{ TRACE; $$ = $1; }
	| postfix_expression '[' expression ']'	{
		TRACE;
		$$ = mk_node(K_DE_REF_OP, $1->src_line, false, 1,
			mk_node('+', $1->src_line, false, 2, $1, $3));
	}
	| postfix_expression '(' ')'	{
		TRACE;
		$$ = mk_node(K_FUNC_CALL_OP, $1->src_line, false, 1, $1);
	}
	| postfix_expression '(' argument_expression_list ')'	{
		TRACE;
		$$ = mk_node(K_FUNC_CALL_OP, $1->src_line, false, 2, $1, $3);
	}
	| postfix_expression '.' IDENTIFIER	{ TRACE; UNSUPPORTED; }
	| postfix_expression PTR_OP IDENTIFIER	{ TRACE; UNSUPPORTED; }
	| postfix_expression INC_OP	{
		TRACE;

		$$ = expand_postfix_op('+', $1);
	}
	| postfix_expression DEC_OP	{
		TRACE;

		$$ = expand_postfix_op('-', $1);
	}
	| '(' type_name ')' '{' initializer_list '}'	{ TRACE; UNSUPPORTED; }
	| '(' type_name ')' '{' initializer_list ',' '}'	{ TRACE; UNSUPPORTED; }
	;

argument_expression_list
	: assignment_expression	{ TRACE; $$ = $1; }
	| argument_expression_list ',' assignment_expression	{ TRACE; $$ = mk_node(',', $1->src_line, false, 2, $1, $3); }
	;

unary_expression
	: postfix_expression	{ TRACE; $$ = $1; }
	| INC_OP unary_expression	{
		TRACE;

		$$ = expand_prefix_op('+', $2);
	}
	| DEC_OP unary_expression	{
		TRACE;

		$$ = expand_prefix_op('-', $2);
	}
	| unary_operator cast_expression	{
		TRACE;

		$$ = mk_node($1.t, $1.line, false, 1, $2);
	}
	| SIZEOF unary_expression	{ TRACE; UNSUPPORTED; }
	| SIZEOF '(' type_name ')'	{ TRACE; UNSUPPORTED; }
	| ALIGNOF '(' type_name ')'	{ TRACE; UNSUPPORTED; }
	;

unary_operator
	: '&'	{ TRACE; $$.t = K_REF_OP; $$.line = $1.line; }
	| '*'	{ TRACE; $$.t = K_DE_REF_OP; $$.line = $1.line; }
	| '+'	{ TRACE; $$.t = '+'; $$.line = $1.line; }
	| '-'	{ TRACE; $$.t = '-'; $$.line = $1.line; }
	| '~'	{ TRACE; $$.t = '~'; $$.line = $1.line; }
	| '!'	{ TRACE; $$.t = '!'; $$.line = $1.line; }
	;

cast_expression
	: unary_expression	{ TRACE; $$ = $1; }
	| '(' type_name ')' cast_expression	{ TRACE; UNSUPPORTED; }
	;

multiplicative_expression
	: cast_expression	{ TRACE; $$ = $1; }
	| multiplicative_expression '*' cast_expression	{ TRACE; $$ = mk_node('*', $2.line, false, 2, $1, $3); }
	| multiplicative_expression '/' cast_expression	{ TRACE; $$ = mk_node('/', $2.line, false, 2, $1, $3); }
	| multiplicative_expression '%' cast_expression	{ TRACE; $$ = mk_node('%', $2.line, false, 2, $1, $3); }
	;

additive_expression
	: multiplicative_expression	{ TRACE; $$ = $1; }
	| additive_expression '+' multiplicative_expression	{ TRACE; $$ = mk_node('+', $2.line, false, 2, $1, $3); }
	| additive_expression '-' multiplicative_expression	{ TRACE; $$ = mk_node('-', $2.line, false, 2, $1, $3); }
	;

shift_expression
	: additive_expression	{ TRACE; $$ = $1; }
	| shift_expression LEFT_OP additive_expression	{ TRACE; $$ = mk_node(K_LEFT_OP, $2.line, false, 2, $1, $3); }
	| shift_expression RIGHT_OP additive_expression	{ TRACE; $$ = mk_node(K_RIGHT_OP, $2.line, false, 2, $1, $3); }
	;

relational_expression
	: shift_expression	{ TRACE; $$ = $1; }
	| relational_expression '<' shift_expression	{ TRACE; $$ = mk_node('<', $2.line, false, 2, $1, $3); }
	| relational_expression '>' shift_expression	{ TRACE; $$ = mk_node('>', $2.line, false, 2, $1, $3); }
	| relational_expression LE_OP shift_expression	{ TRACE; $$ = mk_node(K_LE_OP, $2.line, false, 2, $1, $3); }
	| relational_expression GE_OP shift_expression	{ TRACE; $$ = mk_node(K_GE_OP, $2.line, false, 2, $1, $3); }
	;

equality_expression
	: relational_expression	{ TRACE; $$ = $1; }
	| equality_expression EQ_OP relational_expression	{ TRACE; $$ = mk_node(K_EQ_OP, $2.line, false, 2, $1, $3); }
	| equality_expression NE_OP relational_expression	{ TRACE; $$ = mk_node(K_NE_OP, $2.line, false, 2, $1, $3); }
	;

and_expression
	: equality_expression	{ TRACE; $$ = $1; }
	| and_expression '&' equality_expression	{ TRACE; $$ = mk_node('&', $2.line, false, 2, $1, $3); }
	;

exclusive_or_expression
	: and_expression	{ TRACE; $$ = $1; }
	| exclusive_or_expression '^' and_expression	{ TRACE; $$ = mk_node('^', $2.line, false, 2, $1, $3); }
	;

inclusive_or_expression
	: exclusive_or_expression	{ TRACE; $$ = $1; }
	| inclusive_or_expression '|' exclusive_or_expression	{ TRACE; $$ = mk_node('|', $2.line, false, 2, $1, $3); }
	;

logical_and_expression
	: inclusive_or_expression	{ TRACE; $$ = $1; }
	| logical_and_expression AND_OP inclusive_or_expression	{
		struct node_s *cond[2];

		TRACE;
		cond[0] = expand_bool($1);
		cond[1] = expand_bool($3);
		$$ = mk_node(K_AND_OP, $2.line, false, 2, cond[0], cond[1]);
	}
	;

logical_or_expression
	: logical_and_expression	{ TRACE; $$ = $1; }
	| logical_or_expression OR_OP logical_and_expression {
		struct node_s *cond[2];

		TRACE;
		cond[0] = expand_bool($1);
		cond[1] = expand_bool($3);
		$$ = mk_node(K_OR_OP, $2.line, false, 2, cond[0], cond[1]);
	}
	;

conditional_expression
	: logical_or_expression	{ TRACE; $$ = $1; }
	| logical_or_expression '?' expression ':' conditional_expression	{ TRACE; UNSUPPORTED; }
	;

assignment_expression
	: conditional_expression	{ TRACE; $$ = $1; }
	| unary_expression assignment_operator assignment_expression	{
		TRACE;
		$$ = mk_node($2.oper, $2.line, true, 2, $1, $3);
	}
	;

assignment_operator
	: '='	{ TRACE; $$.oper = '=' ; }
	| MUL_ASSIGN	{ TRACE; $$.oper = K_MUL_ASSIGN_OP; }
	| DIV_ASSIGN	{ TRACE; $$.oper = K_DIV_ASSIGN_OP; }
	| MOD_ASSIGN	{ TRACE; $$.oper = K_MOD_ASSIGN_OP; }
	| ADD_ASSIGN	{ TRACE; $$.oper = K_ADD_ASSIGN_OP; }
	| SUB_ASSIGN	{ TRACE; $$.oper = K_SUB_ASSIGN_OP; }
	| LEFT_ASSIGN	{ TRACE; $$.oper = K_LEFT_ASSIGN_OP; }
	| RIGHT_ASSIGN	{ TRACE; $$.oper = K_RIGHT_ASSIGN_OP; }
	| AND_ASSIGN	{ TRACE; $$.oper = K_AND_ASSIGN_OP; }
	| XOR_ASSIGN	{ TRACE; $$.oper = K_XOR_ASSIGN_OP; }
	| OR_ASSIGN	{ TRACE; $$.oper = K_OR_ASSIGN_OP; }
	;

expression
	: assignment_expression	{ TRACE; $$ = $1; }
	| expression ',' assignment_expression	{ TRACE; UNSUPPORTED; }
	;

constant_expression
	: conditional_expression	{ TRACE; UNSUPPORTED; }	/* with constraints */
	;

declaration
	: declaration_specifiers ';'	{ TRACE; UNSUPPORTED; }
	| declaration_specifiers init_declarator_list ';'	{
		TRACE;
		$$ = mk_node(K_DECLARATION_OP, $1->src_line, false, 2, $1, $2);
	}
	| static_assert_declaration	{ TRACE; UNSUPPORTED; }
	;

declaration_specifiers
	: storage_class_specifier declaration_specifiers	{ TRACE; UNSUPPORTED; }
	| storage_class_specifier	{ TRACE; $$ = $1; }
	| type_specifier declaration_specifiers	{
		TRACE;
		$$ = mk_node(K_DECL_SPEC_OP, $1->src_line, false, 2, $1, $2);
		if (TYPE($1) == TYPE_SPEC_T && $1->c_type.t == LONG_T &&
		    TYPE($2) == TYPE_SPEC_T && $2->c_type.t == LONG_T)
			$$->c_type.t = LONG_LONG_T;
		else if (TYPE($1) == TYPE_SPEC_T && $1->c_type.t == LONG_T)
			$$->c_type.t = LONG_T;
		else if (TYPE($1) == TYPE_SPEC_T &&
			$1->t_spec.type_spec == K_UNSIGNED_T)
			$$->c_type.t = $2->c_type.t + 1;
		else
			$$->c_type.t = $1->c_type.t;
}
	| type_specifier	{ TRACE; $$ = $1; }
	| type_qualifier declaration_specifiers	{ TRACE; UNSUPPORTED; }
	| type_qualifier	{ TRACE; $$ = $1; }
	| function_specifier declaration_specifiers	{ TRACE; UNSUPPORTED; }
	| function_specifier	{ TRACE; $$ = $1; }
	| alignment_specifier declaration_specifiers	{ TRACE; UNSUPPORTED; }
	| alignment_specifier	{ TRACE; $$ = $1; }
	;

init_declarator_list
	: init_declarator	{ TRACE; $$ = $1; }
	| init_declarator_list ',' init_declarator	{ TRACE; UNSUPPORTED; }
	;

init_declarator
	: declarator '=' initializer	{ TRACE; $$ = mk_node('=', $1->src_line, true, 2, $1, $3); }
	| declarator	{ TRACE; $$ = $1; }
	;

storage_class_specifier
	: TYPEDEF	{ TRACE; UNSUPPORTED; }	/* identifiers must be flagged as TYPEDEF_NAME */
	| EXTERN	{ TRACE; UNSUPPORTED; }
	| STATIC	{ TRACE; UNSUPPORTED; }
	| THREAD_LOCAL	{ TRACE; UNSUPPORTED; }
	| AUTO	{ TRACE; UNSUPPORTED; }
	| REGISTER	{ TRACE; UNSUPPORTED; }
	;

type_specifier
	: VOID				{ TRACE; $$ = mk_leaf_type_spec(K_VOID_T, $1.line); }
	| CHAR				{ TRACE; $$ = mk_leaf_type_spec(K_CHAR_T, $1.line); }
	| SHORT				{ TRACE; $$ = mk_leaf_type_spec(K_SHORT_T, $1.line); }
	| INT				{ TRACE; $$ = mk_leaf_type_spec(K_INT_T, $1.line); }
	| LONG				{ TRACE; $$ = mk_leaf_type_spec(K_LONG_T, $1.line); }
	| FLOAT				{ TRACE; $$ = mk_leaf_type_spec(K_FLOAT_T, $1.line); }
	| DOUBLE			{ TRACE; $$ = mk_leaf_type_spec(K_DOUBLE_T, $1.line); }
	| SIGNED			{ TRACE; $$ = mk_leaf_type_spec(K_SIGNED_T, $1.line); }
	| UNSIGNED			{ TRACE; $$ = mk_leaf_type_spec(K_UNSIGNED_T, $1.line); }
	| BOOL				{ TRACE; $$ = mk_leaf_type_spec(K_BOOL_T, $1.line); }
	| COMPLEX			{ TRACE; $$ = mk_leaf_type_spec(K_COMPLEX_T, $1.line); }
	| IMAGINARY			{ TRACE; $$ = mk_leaf_type_spec(K_IMAGINARY_T, $1.line); }	  	/* non-mandated extension */
	| atomic_type_specifier		{ TRACE; UNSUPPORTED; }
	| struct_or_union_specifier	{ TRACE; UNSUPPORTED; }
	| enum_specifier		{ TRACE; UNSUPPORTED; }
	| TYPEDEF_NAME			{ TRACE; UNSUPPORTED; }		/* after it has been defined as such */
	;

struct_or_union_specifier
	: struct_or_union '{' struct_declaration_list '}'	{ TRACE; UNSUPPORTED; }
	| struct_or_union IDENTIFIER '{' struct_declaration_list '}'	{ TRACE; UNSUPPORTED; }
	| struct_or_union IDENTIFIER	{ TRACE; UNSUPPORTED; }
	;

struct_or_union
	: STRUCT	{ TRACE; UNSUPPORTED; }
	| UNION	{ TRACE; UNSUPPORTED; }
	;

struct_declaration_list
	: struct_declaration	{ TRACE; UNSUPPORTED; }
	| struct_declaration_list struct_declaration	{ TRACE; UNSUPPORTED; }
	;

struct_declaration
	: specifier_qualifier_list ';'		{ TRACE; UNSUPPORTED; }/* for anonymous struct/union */
	| specifier_qualifier_list struct_declarator_list ';'	{ TRACE; UNSUPPORTED; }
	| static_assert_declaration	{ TRACE; UNSUPPORTED; }
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list	{ TRACE; UNSUPPORTED; }
	| type_specifier	{ TRACE; UNSUPPORTED; }
	| type_qualifier specifier_qualifier_list	{ TRACE; UNSUPPORTED; }
	| type_qualifier	{ TRACE; UNSUPPORTED; }
	;

struct_declarator_list
	: struct_declarator	{ TRACE; UNSUPPORTED; }
	| struct_declarator_list ',' struct_declarator	{ TRACE; UNSUPPORTED; }
	;

struct_declarator
	: ':' constant_expression	{ TRACE; UNSUPPORTED; }
	| declarator ':' constant_expression	{ TRACE; UNSUPPORTED; }
	| declarator	{ TRACE; UNSUPPORTED; }
	;

enum_specifier
	: ENUM '{' enumerator_list '}'	{ TRACE; UNSUPPORTED; }
	| ENUM '{' enumerator_list ',' '}'	{ TRACE; UNSUPPORTED; }
	| ENUM IDENTIFIER '{' enumerator_list '}'	{ TRACE; UNSUPPORTED; }
	| ENUM IDENTIFIER '{' enumerator_list ',' '}'	{ TRACE; UNSUPPORTED; }
	| ENUM IDENTIFIER	{ TRACE; UNSUPPORTED; }
	;

enumerator_list
	: enumerator	{ TRACE; UNSUPPORTED; }
	| enumerator_list ',' enumerator	{ TRACE; UNSUPPORTED; }
	;

enumerator	/* identifiers must be flagged as ENUMERATION_CONSTANT */
	: enumeration_constant '=' constant_expression	{ TRACE; UNSUPPORTED; }
	| enumeration_constant	{ TRACE; UNSUPPORTED; }
	;

atomic_type_specifier
	: ATOMIC '(' type_name ')'	{ TRACE; UNSUPPORTED; }
	;

type_qualifier
	: CONST	{ TRACE; }
	| RESTRICT	{ TRACE; }
	| VOLATILE	{ TRACE; }
	| ATOMIC	{ TRACE; }
	;

function_specifier
	: INLINE	{ TRACE; }
	| NORETURN	{ TRACE; }
	;

alignment_specifier
	: ALIGNAS '(' type_name ')'	{ TRACE; }
	| ALIGNAS '(' constant_expression ')'	{ TRACE; }
	;

declarator
	: pointer direct_declarator	{
		TRACE;
		$$ = $2;
		$$->c_type.ref_lev = $1->c_type.ref_lev;
	}
	| direct_declarator		{ TRACE; $$ = $1; }
	;

direct_declarator
	: IDENTIFIER	{ TRACE; $$ = mk_leaf_id($1.str, $1.line); }
	| '(' declarator ')'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' '*' ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' STATIC type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' type_qualifier_list '*' ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' type_qualifier_list STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_declarator '[' type_qualifier_list ']'	{ TRACE; UNSUPPORTED; }
| direct_declarator '[' assignment_expression ']'	{ TRACE; $$ = mk_node(K_ARRAY_DECL_OP, $2.line, false, 2, $1, $3); }
	| direct_declarator '(' parameter_type_list ')'	{ TRACE; $$ = mk_node(K_DIRECT_DECL_OP, $2.line, false, 2, $1, $3); }
	| direct_declarator '(' ')'	{ TRACE; $$ = mk_node(K_DIRECT_DECL_OP, $1->src_line, false, 1, $1); }
	| direct_declarator '(' identifier_list ')'	{ TRACE; UNSUPPORTED; }
	;

pointer
	: '*' type_qualifier_list pointer	{ TRACE; UNSUPPORTED; }
	| '*' type_qualifier_list	{ TRACE; UNSUPPORTED; }
	| '*' pointer	{
		TRACE;
		$$ = $2;
		$$->c_type.ref_lev++;
	}
	| '*'	{
		TRACE;
		$$ = mk_leaf_type_spec(K_POINTER_T, $1.line);
	}
	;

type_qualifier_list
	: type_qualifier	{ TRACE; UNSUPPORTED; }
	| type_qualifier_list type_qualifier	{ TRACE; UNSUPPORTED; }
	;


parameter_type_list
	: parameter_list ',' ELLIPSIS	{ TRACE; UNSUPPORTED; }
	| parameter_list	{ TRACE; $$ = $1; }
	;

parameter_list
	: parameter_declaration	{ TRACE; $$ = $1; }
	| parameter_list ',' parameter_declaration	{ TRACE; $$ = mk_node(',', $2.line, false, 2, $1, $3); }
	;

parameter_declaration
	: declaration_specifiers declarator	{ TRACE; $$ = mk_node(K_PARAM_DECL_OP, $1->src_line, false, 2, $1, $2); }
	| declaration_specifiers abstract_declarator	{ TRACE; UNSUPPORTED; }
	| declaration_specifiers	{ TRACE; }
	;

identifier_list
	: IDENTIFIER	{ TRACE; UNSUPPORTED; }
	| identifier_list ',' IDENTIFIER	{ TRACE; UNSUPPORTED; }
	;

type_name
	: specifier_qualifier_list abstract_declarator	{ TRACE; UNSUPPORTED; }
	| specifier_qualifier_list	{ TRACE; UNSUPPORTED; }
	;

abstract_declarator
	: pointer direct_abstract_declarator	{ TRACE; UNSUPPORTED; }
	| pointer	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator	{ TRACE; UNSUPPORTED; }
	;

direct_abstract_declarator
	: '(' abstract_declarator ')'	{ TRACE; UNSUPPORTED; }
	| '[' ']'	{ TRACE; UNSUPPORTED; }
	| '[' '*' ']'	{ TRACE; UNSUPPORTED; }
	| '[' STATIC type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| '[' STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| '[' type_qualifier_list STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| '[' type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| '[' type_qualifier_list ']'	{ TRACE; UNSUPPORTED; }
	| '[' assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' '*' ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' STATIC type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' type_qualifier_list assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' type_qualifier_list STATIC assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' type_qualifier_list ']'	{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '[' assignment_expression ']'	{ TRACE; UNSUPPORTED; }
	| '(' ')'							{ TRACE; UNSUPPORTED; }
	| '(' parameter_type_list ')'					{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '(' ')'				{ TRACE; UNSUPPORTED; }
	| direct_abstract_declarator '(' parameter_type_list ')'	{ TRACE; UNSUPPORTED; }
	;

initializer
	: '{' initializer_list '}'	{ TRACE; UNSUPPORTED; }
	| '{' initializer_list ',' '}'	{ TRACE; UNSUPPORTED; }
	| assignment_expression		{ TRACE; }
	;

initializer_list
	: designation initializer			{ TRACE; UNSUPPORTED; }
	| initializer					{ TRACE; UNSUPPORTED; }
	| initializer_list ',' designation initializer	{ TRACE; UNSUPPORTED; }
	| initializer_list ',' initializer		{ TRACE; UNSUPPORTED; }
	;

designation
	: designator_list '='	{ TRACE; UNSUPPORTED; }
	;

designator_list
	: designator			{ TRACE; UNSUPPORTED; }
	| designator_list designator	{ TRACE; UNSUPPORTED; }
	;

designator
	: '[' constant_expression ']'	{ TRACE; UNSUPPORTED; }
	| '.' IDENTIFIER		{ TRACE; UNSUPPORTED; }
	;

static_assert_declaration
	: STATIC_ASSERT '(' constant_expression ',' STRING_LITERAL ')' ';'	{ TRACE; UNSUPPORTED; }
	;

statement
	: labeled_statement	{ TRACE; $$ = $1; }
	| compound_statement	{ TRACE; $$ = $1; }
	| expression_statement	{ TRACE; $$ = $1; }
	| selection_statement	{ TRACE; $$ = $1; }
	| iteration_statement	{ TRACE; $$ = $1; }
	| jump_statement	{ TRACE; $$ = $1; }
	;

labeled_statement
	: IDENTIFIER ':' statement			{ TRACE; UNSUPPORTED; }
	| CASE constant_expression ':' statement	{
		TRACE;

		$$ = mk_node(K_CASE_OP, $1.line, false, 2, $2, $4);
	}
	| DEFAULT ':' statement				{
		TRACE;

		$$ = mk_node(K_CASE_OP, $1.line, false, 1, $3);
	}
	;

compound_statement
	: '{' '}'			{ TRACE; $$ = NULL; }
	| '{'  block_item_list '}'	{ TRACE; $$ = mk_node(K_CODE_BLOCK_OP, $1.line, false, 1, $2); }
	;

block_item_list
	: block_item		       	{ TRACE; $$ = $1; }
	| block_item_list block_item	{ TRACE; $$ = mk_node(';', $1->src_line, false, 2, $1, $2); }
	;

block_item
	: declaration	{ TRACE; $$ = $1; }
	| statement	{ TRACE; $$ = $1; }
	;

expression_statement
	: ';'	{ TRACE; $$ = NULL; }
	| expression ';'	{ TRACE; $$ = $1; }
	;

selection_statement
	: IF '(' expression ')' statement ELSE statement	{
		struct node_s *cond;

		TRACE;

		cond = expand_bool($3);
		$$ = mk_node(K_IF_ELSE_OP, $1.line, false, 3, cond, $5, $7);
	}
	| IF '(' expression ')' statement			{
		struct node_s *cond;

		TRACE;
		cond = expand_bool($3);
		$$ = mk_node(K_IF_ELSE_OP, $1.line, false, 3, cond, $5, NULL);
	}
	| SWITCH '(' expression ')' statement			{
		TRACE;

		$$ = mk_node(K_SWITCH_OP, $1.line, false, 2, $3, $5);
	}
	;

iteration_statement
	: WHILE '(' expression ')' statement						{
		struct node_s *cond;

		TRACE;
		cond = expand_bool($3);
		$$ = mk_node(K_WHILE_OP, $1.line, false, 2, cond, $5);
	}
	| DO statement WHILE '(' expression ')' ';'					{
		struct node_s *cond;

		TRACE;
		cond = expand_bool($5);
		$$ = mk_node(K_DO_WHILE_OP, $1.line, false, 2, $2, cond);
	}
	| FOR '(' expression_statement expression_statement ')' statement		{
		struct node_s *body;
		TRACE;
		body = mk_node(';', $1.line, false, 2, $4, $6);
		$$ = mk_node(K_WHILE_OP, $1.line, false, 2, $3, body);

	}
	| FOR '(' expression_statement expression_statement expression ')' statement	{
		struct node_s *while_n, *body;
		TRACE;
		body = mk_node(';', $1.line, false, 2, $5, $7);
		while_n = mk_node(K_WHILE_OP, $1.line, false, 2, $4, body);
		$$ = mk_node(';', $1.line, false, 2, $3, while_n);
	}
	| FOR '(' declaration expression_statement ')' statement	{
		struct node_s *while_n;
		TRACE;
		while_n = mk_node(K_WHILE_OP, $1.line, false, 2, $4, $6);
		$$ = mk_node(';', $1.line, false, 2, $3, while_n);
	}
	| FOR '(' declaration expression_statement expression ')' statement	{
		struct node_s *while_n, *body;
		TRACE;
		body = mk_node(';', $1.line, false, 2, $5, $7);
		while_n = mk_node(K_WHILE_OP, $1.line, false, 2, $4, body);
		$$ = mk_node(';', $1.line, false, 2, $3, while_n);
	}
	;

jump_statement
	: GOTO IDENTIFIER ';'		{ TRACE; UNSUPPORTED; }
	| CONTINUE ';'			{ TRACE; UNSUPPORTED; }
	| BREAK ';'			{ TRACE; UNSUPPORTED; }
	| RETURN ';'			{ TRACE; $$ = mk_node(K_RETURN_OP, $1.line, false, 0); }
	| RETURN expression ';'		{ TRACE; $$ = mk_node(K_RETURN_OP, $1.line, false, 1, $2); }
	;

external_declaration
	: function_definition	{ TRACE; $$ = $1; }
	| declaration		{
		TRACE;
		ASSERT(OP($1) == K_DECLARATION_OP);
		SET_OP($1, K_EXT_DECLARATION_OP);
		$$ = $1;
	}
	;

function_definition
	: declaration_specifiers declarator declaration_list compound_statement { TRACE; UNSUPPORTED; }
	| declaration_specifiers declarator compound_statement			{
		TRACE;
		ASSERT(OP($2) == K_DIRECT_DECL_OP);
		$1->c_type.ref_lev = $2->c_type.ref_lev;
		if ($2->op.nops == 1)
			$$ = mk_node(K_FUNC_DECL_OP, $1->src_line, false, 4, $1, CHILD($2, 0), NULL, $3);
		else
			$$ = mk_node(K_FUNC_DECL_OP, $1->src_line, false, 4, $1, CHILD($2, 0), CHILD($2, 1), $3);
	}
	;

declaration_list
	: declaration				{ TRACE; UNSUPPORTED; }
	| declaration_list declaration		{ TRACE; UNSUPPORTED; }
	;

%%
