#include "ir_optim.h"
#include "ir.h"

static void ir_swap_operands(struct ir_statment_s *frag)
{
	struct ir_operand_s tmp;
	tmp = frag->arg[0];
	frag->arg[0] = frag->arg[1];
	frag->arg[1] = tmp;
}

/* remove less less-equal operators + make immediates always 2th argument */
static void ir_canonicalize_cmp(void)
{
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {

		switch ((int)frag->op) {
		case IR_IFEQ_OP:
		case IR_IFNEQ_OP:
		case IR_IFGE_OP:
		case IR_IFLE_OP:
		case IR_IFGEQ_OP:
		case IR_IFLEQ_OP:
			if (frag->arg[0].type == IR_CONST) {
				ASSERT(frag->arg[1].type == IR_SYM);
				frag->op = ir_reverse_cmp_op(frag->op);
				ir_swap_operands(frag);
			}
			break;
		default:
			break;
		}

		if (frag->op == '<'  ||
		    frag->op == IR_IFLEQ_OP) {
			    if (frag->arg[0].type == IR_SYM &&
				frag->arg[1].type == IR_SYM) {
				    frag->op = ir_reverse_cmp_op(frag->op);
				    ir_swap_operands(frag);
			    }
			    else {
				    char *l;
				    struct ir_statment_s *new_frag;

				    if (frag->next->type == IR_FRAG_LABEL) {
					    l = frag->next->label;
				    } else {
					    l = gen_label("opt");
					    new_frag = frag_alloc();
					    new_frag->type = IR_FRAG_LABEL;
					    new_frag->label = l;
					    ir_add_frag_after(new_frag, frag);
				    }

				    new_frag = frag_alloc();
				    new_frag->op = IR_GOTO_OP;
				    new_frag->res.type = IR_LABEL;
				    frag->op = ir_invert_cmp_op(frag->op);
				    new_frag->res.label = frag->res.label;
				    frag->res.label = l;

				    ir_add_frag_after(new_frag, frag);
			    }
		}

		frag = frag->next;
	}
}

/* remove jumps to consecutive lables */
static void ir_gotos_clean_up_1(void)
{
	struct ir_statment_s *p;
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {
		if (frag->type == IR_FRAG_INST && frag->op == IR_GOTO_OP) {
			p = frag->next;
			while (p && p->type == IR_FRAG_LABEL) {
				if (frag->res.label == p->label) {
					ir_frag_rm(frag);
					frag = p;
					break;
				}
				p = p->next;
			}
		}

		frag = frag->next;
	}
}

/* remove the unreachable jumps that follows directly ret insts */
static void ir_gotos_clean_up_2(void)
{
	struct ir_statment_s *p;
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {
		if (frag->type == IR_FRAG_INST && frag->op == IR_RET_OP) {
			p = frag->next;
			if (p && p->type == IR_FRAG_INST && p->op == IR_GOTO_OP)
				ir_frag_rm(p);
		}

		frag = frag->next;
	}
}

/*
if (sizeof(p) == 4)
+ $0 p 8
LOAD $1 $0    =>   LOAD $1 p 2
 */
/*
static void ir_smart_load_store(void)
{
	struct ir_statment_s *prev;
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {
		prev = frag->prev;
		if (frag->type == IR_FRAG_INST &&
		    (frag->op == IR_LOAD_OP || frag->op == IR_STORE_OP) &&
		    prev &&prev->type == IR_FRAG_INST && prev->op == '+' &&
		    prev->res.sym == frag->arg[0].sym) {
			ASSERT(!(prev->arg[1].imm.val.u_int64 %
				 type_base_size(&prev->arg[0].sym->c_type)));

			ASSERT(frag->arg[0].type == IR_SYM);
			frag->arg[0].sym = prev->arg[0].sym;
			frag->arg[1].type = IR_CONST;
			frag->arg[1].imm.val.u_int64 = prev->arg[1].imm.val.u_int64;
			ir_frag_rm(prev);
		}
		frag = frag->next;
	}
}
*/

/*
  clean-up unusefull assign instructions
  OP $1 x x  => OP $2 x x
  =  $2 $1
 */
static void ir_clean_assign(void)
{
	struct ir_statment_s *prev;
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {
		prev = frag->prev;
		if (frag->type == IR_FRAG_INST && frag->op == '=' &&
		    frag->prev->res.death &&
		    prev && prev->type == IR_FRAG_INST  &&
		    frag->arg[0].type == IR_SYM &&
		    prev->res.sym == frag->arg[0].sym) {
			prev->res.sym = frag->res.sym;
			frag = frag->next;
			ir_frag_rm(frag->prev);
			continue;
		}
		if (frag->type == IR_FRAG_INST && frag->op == '=' &&
		    frag->res.death) {
			ir_frag_rm(frag);
		}
		frag = frag->next;
	}
}

/*
  used by ir_liveness
 */
static bool ir_possibly_in_loop(struct ir_statment_s *frag)
{
	while ((frag = frag->next)) {
		if (frag->type == IR_FRAG_INST) {
			if (frag->op == IR_ENDFUNC_OP)
				return false;
			if (ir_is_frag_branch(frag))
				return true;
		}
	}

	return false;
}

/*
   basic liveness analysis
   this pass has to be invoked as last
*/
static void ir_liveness(void)
{
	struct ir_statment_s *frag = ir_code.first;

	while (frag) {
		if (frag->type == IR_FRAG_INST) {
			if (frag->res.type == IR_SYM)
				frag->res.sym->ir_last = (void *)frag;
			if (frag->arg[0].type == IR_SYM)
				frag->arg[0].sym->ir_last = (void *)frag;
			if (frag->arg[1].type == IR_SYM)
				frag->arg[1].sym->ir_last = (void *)frag;
		}
		frag = frag->next;
	}

	frag = ir_code.first;

	while (frag) {
		if (frag->type == IR_FRAG_INST && !ir_possibly_in_loop(frag)) {
			if (frag->res.type == IR_SYM &&
			    frag->res.sym->ir_last == (void *)frag)
				frag->res.death = true;
			if (frag->arg[0].type == IR_SYM &&
			    frag->arg[0].sym->ir_last == (void *)frag)
				frag->arg[0].death = true;
			if (frag->arg[1].type == IR_SYM &&
			    frag->arg[1].sym->ir_last == (void *)frag)
				frag->arg[1].death = true;
		}
		frag = frag->next;
	}
}

void ir_optimizer(void)
{
	ir_canonicalize_cmp();
	ir_gotos_clean_up_1();
	ir_gotos_clean_up_2();
	/* ir_smart_load_store(); */
	ir_liveness();
	ir_clean_assign();
	ir_liveness();
}
