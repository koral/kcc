#include "kpu_peephole.h"
#include "kpu_gen.h"

static void kpu_frag_rm(struct kpu_frag_s *k_frag)
{
        struct kpu_frag_s *prev = k_frag->prev;
        struct kpu_frag_s *next = k_frag->next;

        if (prev)
		prev->next = next;
	if (next)
		next->prev = prev;

	kpu_free_frag(k_frag);
}

static void kpu_frag_swap(struct kpu_frag_s *k_frag1, struct kpu_frag_s *k_frag2)
{

        struct kpu_frag_s *prev = k_frag1->prev;
        struct kpu_frag_s *next = k_frag2->next;

        k_frag1->next = next;
        k_frag1->prev = k_frag2;

        k_frag2->next = k_frag1;
        k_frag2->prev = prev;

        if (prev)
                prev->next = k_frag2;
        if (next)
                next->prev = k_frag1;
}

/* remove unnecessary jumps to following labels. */
static void kpu_jmp_cleanup(void)
{
        struct kpu_frag_s *next;
	struct kpu_frag_s *k_frag = kpu_code.first;

	while (k_frag) {
		if (k_frag->type == KPU_FRAG_INST && k_frag->op == KPU_jmp) {
                        next = k_frag->next;
			if (next &&
			    next->type == KPU_FRAG_LABEL &&
			    !strcmp(next->label, k_frag->arg[0].label)) {
                                kpu_frag_rm(k_frag);
                                k_frag = next;
                                continue;
                        }
                        if (next &&
			    next->type == KPU_FRAG_INST &&
                            next->op == KPU_nop &&
                            next->next &&
                            next->next->type == KPU_FRAG_LABEL &&
			    !strcmp(next->next->label, k_frag->arg[0].label)) {
                                struct kpu_frag_s *tmp = next->next;
                                kpu_frag_rm(k_frag);
                                kpu_frag_rm(next);
                                k_frag = tmp;
                                continue;
                        }
		}
		k_frag = k_frag->next;
	}
}

/* does some simple reordering trying to make use of the branch delay slot. */
void kpu_delay_slot(void)
{
        struct kpu_frag_s *prev;
        struct kpu_frag_s *next;
        struct kpu_frag_s *k_frag = kpu_code.first;

	while (k_frag) {
                if (k_frag->type == KPU_FRAG_INST &&
                    (k_frag->op == KPU_jmp || k_frag->op == KPU_jmpr ||
                     k_frag->op == KPU_call)) {
                        prev = k_frag->prev;
                        next = k_frag->next;
                        if (next && prev &&
                            next->type == KPU_FRAG_INST &&
                            next->op == KPU_nop &&
                            next->type == KPU_FRAG_INST) {
                                if (prev->type == KPU_FRAG_INST &&
                                    prev->op == KPU_nop)
                                        break;
                                kpu_frag_swap(prev, k_frag);
                                kpu_frag_rm(next);
                        }
                }
                k_frag = k_frag->next;
        }
}

void kpu_asm_peepholes(void)
{
	kpu_jmp_cleanup();
        kpu_delay_slot();
}
